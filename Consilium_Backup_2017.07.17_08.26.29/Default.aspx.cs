﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

namespace Consilium
{
    public partial class _Default : Page
    {
        string myConnectionString = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCurrentUser();
            }
            else
            {

            }
        }

 

        protected void LoadCurrentUser()
        {
            string CurrentUserName = string.Empty;

            //AD Code Here
            CurrentUserName = Request.QueryString["USERNAME"];
            if (string.IsNullOrEmpty(CurrentUserName))
            {
                CurrentUserName = "BHOLMAN";
            }
            Session["USERNAME"] = CurrentUserName;

            string strUserID = GetUserID(CurrentUserName);
            Session["USERID"] = strUserID;

            SetTabVisbility(CurrentUserName);


        }

        private string GetUserID(string UserName)
        {
            string selectSQL = "SELECT ID from Users where UserID = '" + UserName + "'";
            
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strUserID = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strUserID = reader["ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            return strUserID;
        }

        private void SetTabVisbility(string UserName)
        {
            string selectSQL = "SELECT RoleName from vwUserRoles where UserID = '" + UserName + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strRole = string.Empty;
            bool bUser = false;
            bool bReviewer = false;
            bool bFinance = false;


            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strRole = reader["RoleName"].ToString();
                    switch (strRole.ToUpper())
                    {
                        case "USER":
                            bUser = true;
                            break;
                        case "REVIEWER":
                            bReviewer = true;
                            break;
                        case "FINANCE":
                            bFinance = true;
                            break;
                    }
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            RadTab rootTabUser = RadTabStrip1.FindTabByText("Current Year Forecast");
            rootTabUser.Visible = bUser;
            rootTabUser = RadTabStrip1.FindTabByText("Next Year Budget");
            rootTabUser.Visible = bUser;
            rootTabUser = RadTabStrip1.FindTabByText("Budget Comparison");
            rootTabUser.Visible = bUser;
            rootTabUser = RadTabStrip1.FindTabByText("Current Year Budget");
            rootTabUser.Visible = bUser;
            RadTab rootTabReviewer = RadTabStrip1.FindTabByText("Management Budget Review");
            rootTabReviewer.Visible = bReviewer;
            RadTab rootTabFinance = RadTabStrip1.FindTabByText("Finance Budget Review");
            rootTabFinance.Visible = bFinance;
            rootTabFinance = RadTabStrip1.FindTabByText("Finance Forecast Review");
            rootTabFinance.Visible = bFinance;

            if (!bUser && bReviewer)
            {
                RadPageView5.Selected = true;
                rootTabReviewer.Selected = true;
            }
            if (!bUser && !bReviewer)
            {
                RadPageView6.Selected = true;
                rootTabFinance.Selected = true;
            }
        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            GridTableView masterTable = RadGrid1.MasterTableView;
            GridColumn descriptionColumn = masterTable.GetColumnSafe("Notes") as GridColumn;
            (masterTable.GetBatchColumnEditor(descriptionColumn) as GridTextBoxColumnEditor).TextBoxControl.Width = Unit.Percentage(150);

            descriptionColumn = masterTable.GetColumnSafe("Jan") as GridColumn;
            RadNumericTextBox descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Feb") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Mar") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Apr") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("May") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Jun") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Jul") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Aug") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Sep") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Oct") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Nov") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Dec") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            ((GridBoundColumn)RadGrid1.Columns[0]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[1]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[1]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridBoundColumn)RadGrid1.Columns[2]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[2]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridBoundColumn)RadGrid1.Columns[3]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[3]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridBoundColumn)RadGrid1.Columns[4]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[4]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridBoundColumn)RadGrid1.Columns[5]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[5]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridBoundColumn)RadGrid1.Columns[6]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[6]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridBoundColumn)RadGrid1.Columns[7]).ReadOnly = true;
            ((GridBoundColumn)RadGrid1.Columns[7]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridBoundColumn)RadGrid1.Columns[8]).ReadOnly = false;
            ((GridBoundColumn)RadGrid1.Columns[9]).ReadOnly = false;
            ((GridBoundColumn)RadGrid1.Columns[10]).ReadOnly = false;
            ((GridBoundColumn)RadGrid1.Columns[11]).ReadOnly = false;
            ((GridBoundColumn)RadGrid1.Columns[12]).ReadOnly = false;
            ((GridBoundColumn)RadGrid1.Columns[13]).ReadOnly = false;

            ((GridBoundColumn)RadGrid1.Columns[13]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridCalculatedColumn)RadGrid1.Columns[14]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            ((GridCalculatedColumn)RadGrid1.Columns[15]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
        }

            protected void RadGrid2_PreRender(object sender, EventArgs e)

        {

            GridTableView masterTable = RadGrid2.MasterTableView;
            GridColumn descriptionColumn = masterTable.GetColumnSafe("Detail") as GridColumn;
            (masterTable.GetBatchColumnEditor(descriptionColumn) as GridTextBoxColumnEditor).TextBoxControl.Width = Unit.Percentage(110);

            descriptionColumn = masterTable.GetColumnSafe("Quantity") as GridColumn;
            RadNumericTextBox descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("UnitCost") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Jan") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Feb") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Mar") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Apr") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("May") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Jun") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Jul") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Aug") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Sep") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Oct") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Nov") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

            descriptionColumn = masterTable.GetColumnSafe("Dec") as GridColumn;
            descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
            descriptionEditor2.Width = Unit.Percentage(100);
            descriptionEditor2.Style.Add("text-align", "right");

        }


        protected void RadGrid3_PreRender(object sender, EventArgs e)
        {
        }



            private void NotifyUser(string message)

        {

            RadListBoxItem commandListItem = new RadListBoxItem();

            commandListItem.Text = message;

        }
    }
}