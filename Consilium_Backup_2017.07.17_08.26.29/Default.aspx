﻿<%@ Page Title="Consilium" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Consilium._Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

            <script type="text/javascript"> 

                function OnCommand(sender, args)
                {
                    var grid = $find('<%=RadGrid2.ClientID%>');
                    var totalAmount = 0;
                    if (grid) {
                        if (args.get_commandName() == "Distribute") {
                            var rowIndex = 0;
                            var MasterTable = grid.get_masterTableView();
                            var Rows = MasterTable.get_dataItems();
                            var Rows = MasterTable.get_dataItems();
                            var row = Rows[rowIndex];
                            var cell = MasterTable.getCellByColumnUniqueName(row, "ExtendedCost");
                            var ExtendedCost = cell.innerText;
                            var number = Number(ExtendedCost.replace(/[^0-9\.]+/g, ""));
                            var distributeamount = number / 12;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });
                            alert("jimmy");
                            var cellTotal = row.getCellByColumnUniqueName("Jan");
                            alert(cellTotal);
                            cellTotal.innerText = "100";


                            args.set_cancel(true);
                        }
                    }
                }

                function ValueChangedRadGrid1(sender, args) {
                    var grid = $find('<%=RadGrid1.ClientID%>');
                    var totalAmount = 0;
                    if (grid) {
                        var MasterTable = grid.get_masterTableView();
                        var Rows = MasterTable.get_dataItems();
                        for (var i = 0; i < Rows.length; i++) {
                            var row = Rows[i];

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jan");
                            var Jan = cell.innerText;

                            Jan = Number(Jan.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Feb");
                            var Feb = cell.innerText;

                            Feb = Number(Feb.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Mar");
                            var Mar = cell.innerText;

                            Mar = Number(Mar.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Apr");
                            var Apr = cell.innerText;

                            Apr = Number(Apr.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "May");
                            var May = cell.innerText;

                            May = Number(May.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jun");
                            var Jun = cell.innerText;

                            Jun = Number(Jun.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jul");
                            var Jul = cell.innerText;

                            Jul = Number(Jul.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Aug");
                            var Aug = cell.innerText;

                            Aug = Number(Aug.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Sep");
                            var Sep = cell.innerText;

                            Sep = Number(Sep.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Oct");
                            var Oct = cell.innerText;

                            Oct = Number(Oct.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Nov");
                            var Nov = cell.innerText;

                            Nov = Number(Nov.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Dec");
                            var Dec = cell.innerText;

                            Dec = Number(Dec.replace(/[^0-9\.]+/g, ""));

                            Total = Jan + Feb + Mar + Apr + May + Jun + Jul + Aug + Sep + Oct + Nov + Dec;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });

                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "CYTotal");
                            cellTotal.innerText = formatter.format(Total);

                            cell = MasterTable.getCellByColumnUniqueName(row, "CYBudget");
                            var CYBudget = cell.innerText;

                            CYBudget = Number(CYBudget.replace(/[^0-9\.]+/g, ""));

                            Total = CYBudget - Total;

                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "Variance");
                            cellTotal.innerText = formatter.format(Total);



                        }
                    }
                }


                function ValueChangedRadGrid2(sender, args) {
                    var grid = $find('<%=RadGrid2.ClientID%>');
                    var totalAmount = 0;
                    if (grid) {
                        var MasterTable = grid.get_masterTableView();
                        var Rows = MasterTable.get_dataItems();
                        for (var i = 0; i < Rows.length; i++) {
                            var row = Rows[i];
                            var cell = MasterTable.getCellByColumnUniqueName(row, "Quantity");
                            var Quantity = cell.innerText;

                            cell = MasterTable.getCellByColumnUniqueName(row, "UnitCost");
                            var UnitCost = cell.innerText;

                            var number = Number(UnitCost.replace(/[^0-9\.]+/g, ""));

                            var Total = Quantity * number;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });

                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "ExtendedCost");
                            cellTotal.innerText = formatter.format(Total);

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jan");
                            var Jan = cell.innerText;

                            Jan = Number(Jan.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Feb");
                            var Feb = cell.innerText;

                            Feb = Number(Feb.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Mar");
                            var Mar = cell.innerText;

                            Mar = Number(Mar.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Apr");
                            var Apr = cell.innerText;

                            Apr = Number(Apr.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "May");
                            var May = cell.innerText;

                            May = Number(May.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jun");
                            var Jun = cell.innerText;

                            Jun = Number(Jun.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jul");
                            var Jul = cell.innerText;

                            Jul = Number(Jul.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Aug");
                            var Aug = cell.innerText;

                            Aug = Number(Aug.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Sep");
                            var Sep = cell.innerText;

                            Sep = Number(Sep.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Oct");
                            var Oct = cell.innerText;

                            Oct = Number(Oct.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Nov");
                            var Nov = cell.innerText;

                            Nov = Number(Nov.replace(/[^0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Dec");
                            var Dec = cell.innerText;

                            Dec = Number(Dec.replace(/[^0-9\.]+/g, ""));

                            Total = Jan + Feb + Mar + Apr + May + Jun + Jul + Aug + Sep + Oct + Nov + Dec;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });

                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "Total");
                            cellTotal.innerText = formatter.format(Total);
                            
                        }
                    }
                }

            </script>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1"></telerik:RadAjaxLoadingPanel>
    <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecorationZoneID="demo" DecoratedControls="All" EnableRoundedCorners="false" />
    <h2 style="color:#0091ba">Consilium - Budget Entry and Review Application</h2>
    <br />
    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Office2007">
        <Tabs>
            <telerik:RadTab Text="Current Year Forecast" Width="250px"></telerik:RadTab>
            <telerik:RadTab Text="Next Year Budget" Width="250px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="Budget Comparison" Width="250px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="Current Year Budget" Width="250px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="Management Budget Review" Width="250px"></telerik:RadTab>
            <telerik:RadTab Text="Finance Budget Review" Width="250px"></telerik:RadTab>
            <telerik:RadTab Text="Finance Forecast Review" Width="250px"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0" CssClass="outerMultiPage">
        <telerik:RadPageView runat="server" ID="RadPageView1">
            <telerik:RadGrid ID="RadGrid1" runat="server" AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="False" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" DataSourceID="SqlDataSource1" GridLines="Both" OnPreRender="RadGrid1_PreRender" PageSize="20" Skin="Office2007">
                <ClientSettings>
                    <ClientEvents OnBatchEditCellValueChanged="ValueChangedRadGrid1"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" DataSourceID="SqlDataSource1" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ID" SortOrder="Descending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  FilterControlWidth="600px">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jan" UniqueName="Jan">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Feb" UniqueName="Feb">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Mar" UniqueName="Mar">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Apr" UniqueName="Apr">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="May" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="May" UniqueName="May">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jun" UniqueName="Jun">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jul" UniqueName="Jul">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Aug" UniqueName="Aug">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Sep" UniqueName="Sep">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Oct" UniqueName="Oct">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Nov" UniqueName="Nov">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Dec" UniqueName="Dec">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="CYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Current Year Budget" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="CYBudget" UniqueName="CYBudget" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}+{1}+{2}+{3}+{4}+{5}+{6}+{7}+{8}+{9}+{10}+{11}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year TOTAL" ItemStyle-HorizontalAlign="Right" SortExpression="CYTotal" UniqueName="CYTotal">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridCalculatedColumn DataFields="CYBudget, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}-{10}-{11}-{12}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Variance" ItemStyle-HorizontalAlign="Right" SortExpression="Variance" UniqueName="Variance">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridBoundColumn DataField="Notes" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Notes" UniqueName="Notes" FilterControlWidth="600px">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>

        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2">
            <telerik:RadGrid ID="RadGrid2" runat="server" AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" DataSourceID="SqlDataSource2" GridLines="Both" OnPreRender="RadGrid2_PreRender" PageSize="20" Skin="Office2007">
                <ClientSettings>
                    <ClientEvents OnBatchEditCellValueChanged="ValueChangedRadGrid2" OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" DataSourceID="SqlDataSource2" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ID" SortOrder="Descending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridTemplateColumn DataField="AccountID" DefaultInsertValue="" HeaderStyle-Width="250px" HeaderText="Account Code" UniqueName="AccountID" AllowSorting="true">
                            <ItemTemplate>
                                <%# Eval("AccountCode") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadDropDownList ID="AccountsIDDropDown" runat="server" DataSourceID="SqlDataSourceAccount" DataTextField="AccountName" DataValueField="AccountID" RenderMode="Lightweight" Skin="Office2007" Width="395px">
                                </telerik:RadDropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Detail" HeaderStyle-Width="125px" HeaderText="Detail" SortExpression="Detail" UniqueName="Detail">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity">
                            <HeaderStyle Width="40px" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="UnitCost" UniqueName="UnitCost">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}*{1}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" SortExpression="ExtendedCost" UniqueName="ExtendedCost">
                        </telerik:GridCalculatedColumn>
<%--                        <telerik:GridButtonColumn CommandName="Distribute" Text="Distribute" ButtonType="PushButton" HeaderText="Distribute" HeaderStyle-Width="55px" HeaderStyle-HorizontalAlign="Center">
                            
                        </telerik:GridButtonColumn>--%>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jan" UniqueName="Jan">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Feb" UniqueName="Feb">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Mar" UniqueName="Mar">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Apr" UniqueName="Apr">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="May" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="May" UniqueName="May">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jun" UniqueName="Jun">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jul" UniqueName="Jul">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Aug" UniqueName="Aug">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Sep" UniqueName="Sep">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Oct" UniqueName="Oct">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Nov" UniqueName="Nov">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Dec" UniqueName="Dec">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}+{1}+{2}+{3}+{4}+{5}+{6}+{7}+{8}+{9}+{10}+{11}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" SortExpression="Total" UniqueName="Total">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridButtonColumn CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Delete this product?" ConfirmTitle="Delete" HeaderStyle-Width="50px" HeaderText="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="50px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView3">
            <telerik:RadGrid ID="RadGrid3" runat="server" AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" DataSourceID="SqlDataSource2" GridLines="Both" OnPreRender="RadGrid2_PreRender" PageSize="20" Skin="Office2007">
                <ClientSettings>
                    <ClientEvents OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" DataSourceID="SqlDataSource2" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ID" SortOrder="Descending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridTemplateColumn DataField="AccountID" DefaultInsertValue="" HeaderStyle-Width="250px" HeaderText="Account Code" UniqueName="AccountID" AllowSorting="true">
                            <ItemTemplate>
                                <%# Eval("AccountCode") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadDropDownList ID="AccountsIDDropDown" runat="server" DataSourceID="SqlDataSourceAccount" DataTextField="AccountName" DataValueField="AccountID" RenderMode="Lightweight" Skin="Office2007" Width="395px">
                                </telerik:RadDropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Detail" HeaderStyle-Width="125px" HeaderText="Detail" SortExpression="Detail" UniqueName="Detail">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity">
                            <HeaderStyle Width="40px" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="UnitCost" UniqueName="UnitCost">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}*{1}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" SortExpression="ExtendedCost" UniqueName="ExtendedCost">
                        </telerik:GridCalculatedColumn>
<%--                        <telerik:GridButtonColumn CommandName="Distribute" Text="Distribute" ButtonType="PushButton" HeaderText="Distribute" HeaderStyle-Width="55px" HeaderStyle-HorizontalAlign="Center">
                            
                        </telerik:GridButtonColumn>--%>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jan" UniqueName="Jan">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Feb" UniqueName="Feb">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Mar" UniqueName="Mar">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Apr" UniqueName="Apr">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="May" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="May" UniqueName="May">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jun" UniqueName="Jun">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jul" UniqueName="Jul">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Aug" UniqueName="Aug">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Sep" UniqueName="Sep">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Oct" UniqueName="Oct">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Nov" UniqueName="Nov">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Dec" UniqueName="Dec">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}+{1}+{2}+{3}+{4}+{5}+{6}+{7}+{8}+{9}+{10}+{11}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" SortExpression="Total" UniqueName="Total">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridButtonColumn CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Delete this product?" ConfirmTitle="Delete" HeaderStyle-Width="50px" HeaderText="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="50px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>

        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView4">
            <telerik:RadGrid ID="RadGrid4" runat="server" AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" DataSourceID="SqlDataSource2" GridLines="Both" OnPreRender="RadGrid2_PreRender" PageSize="20" Skin="Office2007">
                <ClientSettings>
                    <ClientEvents OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" DataSourceID="SqlDataSource2" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ID" SortOrder="Descending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridTemplateColumn DataField="AccountID" DefaultInsertValue="" HeaderStyle-Width="250px" HeaderText="Account Code" UniqueName="AccountID" AllowSorting="true">
                            <ItemTemplate>
                                <%# Eval("AccountCode") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadDropDownList ID="AccountsIDDropDown" runat="server" DataSourceID="SqlDataSourceAccount" DataTextField="AccountName" DataValueField="AccountID" RenderMode="Lightweight" Skin="Office2007" Width="395px">
                                </telerik:RadDropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Detail" HeaderStyle-Width="125px" HeaderText="Detail" SortExpression="Detail" UniqueName="Detail">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity">
                            <HeaderStyle Width="40px" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="UnitCost" UniqueName="UnitCost">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}*{1}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" SortExpression="ExtendedCost" UniqueName="ExtendedCost">
                        </telerik:GridCalculatedColumn>
<%--                        <telerik:GridButtonColumn CommandName="Distribute" Text="Distribute" ButtonType="PushButton" HeaderText="Distribute" HeaderStyle-Width="55px" HeaderStyle-HorizontalAlign="Center">
                            
                        </telerik:GridButtonColumn>--%>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jan" UniqueName="Jan">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Feb" UniqueName="Feb">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Mar" UniqueName="Mar">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Apr" UniqueName="Apr">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="May" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="May" UniqueName="May">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jun" UniqueName="Jun">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jul" UniqueName="Jul">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Aug" UniqueName="Aug">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Sep" UniqueName="Sep">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Oct" UniqueName="Oct">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Nov" UniqueName="Nov">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Dec" UniqueName="Dec">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}+{1}+{2}+{3}+{4}+{5}+{6}+{7}+{8}+{9}+{10}+{11}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" SortExpression="Total" UniqueName="Total">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridButtonColumn CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Delete this product?" ConfirmTitle="Delete" HeaderStyle-Width="50px" HeaderText="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="50px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>

        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView5">
            <telerik:RadGrid ID="RadGrid5" runat="server" AllowAutomaticDeletes="False" AllowSorting="true" AllowAutomaticInserts="False" AllowAutomaticUpdates="False" AllowPaging="True" AllowFilteringByColumn="true" IsFilterItemExpanded="false"  AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" DataSourceID="SqlDataSource5" GridLines="Both" PageSize="20" Skin="Office2007">
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="None" DataKeyNames="ID" DataSourceID="SqlDataSource5" EditMode="Batch" HorizontalAlign="NotSet">
                    <CommandItemSettings ShowRefreshButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ID" SortOrder="Ascending" />
                    </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="InputUserID" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="User Name" UniqueName="InputUserID" ReadOnly="true" FilterControlWidth="400px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  FilterControlWidth="600px">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity" ReadOnly="true" AllowFiltering="false"  >
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="UnitCost" UniqueName="UnitCost" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}*{1}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" SortExpression="ExtendedCost" UniqueName="ExtendedCost"  AllowFiltering="false" >
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jan" UniqueName="Jan" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Feb" UniqueName="Feb" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Mar" UniqueName="Mar" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Apr" UniqueName="Apr" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="May" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="May" UniqueName="May" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jun" UniqueName="Jun" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jul" UniqueName="Jul" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Aug" UniqueName="Aug" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Sep" UniqueName="Sep" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Oct" UniqueName="Oct" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Nov" UniqueName="Nov" ReadOnly="true" AllowFiltering="false" >
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Dec" UniqueName="Dec" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}+{1}+{2}+{3}+{4}+{5}+{6}+{7}+{8}+{9}+{10}+{11}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" SortExpression="Total" UniqueName="Total"  AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                     </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView6">
            <telerik:RadGrid ID="RadGrid6" runat="server" AllowAutomaticDeletes="False" AllowSorting="true" AllowAutomaticInserts="False" AllowAutomaticUpdates="False" AllowPaging="True" AllowFilteringByColumn="true" IsFilterItemExpanded="false"  AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" DataSourceID="SqlDataSource6" GridLines="Both" PageSize="20" Skin="Office2007">
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="None" DataKeyNames="ID" DataSourceID="SqlDataSource6" EditMode="Batch" HorizontalAlign="NotSet">
                    <CommandItemSettings ShowRefreshButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ID" SortOrder="Ascending" />
                    </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="InputUserID" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="User Name" UniqueName="InputUserID" ReadOnly="true" FilterControlWidth="400px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  FilterControlWidth="600px">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity" ReadOnly="true" AllowFiltering="false"  >
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="UnitCost" UniqueName="UnitCost" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}*{1}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" SortExpression="ExtendedCost" UniqueName="ExtendedCost"  AllowFiltering="false" >
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jan" UniqueName="Jan" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Feb" UniqueName="Feb" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Mar" UniqueName="Mar" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Apr" UniqueName="Apr" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="May" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="May" UniqueName="May" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jun" UniqueName="Jun" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jul" UniqueName="Jul" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Aug" UniqueName="Aug" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Sep" UniqueName="Sep" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Oct" UniqueName="Oct" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Nov" UniqueName="Nov" ReadOnly="true" AllowFiltering="false" >
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Dec" UniqueName="Dec" ReadOnly="true"  AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}+{1}+{2}+{3}+{4}+{5}+{6}+{7}+{8}+{9}+{10}+{11}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" SortExpression="Total" UniqueName="Total"  AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                     </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView7">
            <telerik:RadGrid ID="RadGrid7" runat="server" AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" DataSourceID="SqlDataSource2" GridLines="Both" OnPreRender="RadGrid2_PreRender" PageSize="20" Skin="Office2007">
                <ClientSettings>
                    <ClientEvents OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" DataSourceID="SqlDataSource2" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ID" SortOrder="Descending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridTemplateColumn DataField="AccountID" DefaultInsertValue="" HeaderStyle-Width="250px" HeaderText="Account Code" UniqueName="AccountID" AllowSorting="true">
                            <ItemTemplate>
                                <%# Eval("AccountCode") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadDropDownList ID="AccountsIDDropDown" runat="server" DataSourceID="SqlDataSourceAccount" DataTextField="AccountName" DataValueField="AccountID" RenderMode="Lightweight" Skin="Office2007" Width="395px">
                                </telerik:RadDropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Detail" HeaderStyle-Width="125px" HeaderText="Detail" SortExpression="Detail" UniqueName="Detail">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity">
                            <HeaderStyle Width="40px" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="UnitCost" UniqueName="UnitCost">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}*{1}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" SortExpression="ExtendedCost" UniqueName="ExtendedCost">
                        </telerik:GridCalculatedColumn>
<%--                        <telerik:GridButtonColumn CommandName="Distribute" Text="Distribute" ButtonType="PushButton" HeaderText="Distribute" HeaderStyle-Width="55px" HeaderStyle-HorizontalAlign="Center">
                            
                        </telerik:GridButtonColumn>--%>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jan" UniqueName="Jan">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Feb" UniqueName="Feb">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Mar" UniqueName="Mar">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Apr" UniqueName="Apr">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="May" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="May" UniqueName="May">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jun" UniqueName="Jun">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Jul" UniqueName="Jul">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Aug" UniqueName="Aug">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Sep" UniqueName="Sep">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Oct" UniqueName="Oct">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Nov" UniqueName="Nov">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" NumericType="Currency" SortExpression="Dec" UniqueName="Dec">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:c}" DataType="System.Decimal" Expression="{0}+{1}+{2}+{3}+{4}+{5}+{6}+{7}+{8}+{9}+{10}+{11}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" SortExpression="Total" UniqueName="Total">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridButtonColumn CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Delete this product?" ConfirmTitle="Delete" HeaderStyle-Width="50px" HeaderText="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="50px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>

        </telerik:RadPageView>
    </telerik:RadMultiPage>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, CYBudget, USERID,AccountID,AccountCode,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Notes FROM vwActualDetail where Userid = @USERNAME and ID is not null"
        UpdateCommand="UpdateActualDetails"
        UpdateCommandType="StoredProcedure">
        <UpdateParameters>
            <asp:Parameter Name="ID" />
            <asp:Parameter Name="Jan" />    
            <asp:Parameter Name="Feb" />
            <asp:Parameter Name="Mar" />
            <asp:Parameter Name="Apr" />
            <asp:Parameter Name="May" />
            <asp:Parameter Name="Jun" />
            <asp:Parameter Name="Jul" />
            <asp:Parameter Name="Aug" />
            <asp:Parameter Name="Sep" />
            <asp:Parameter Name="Oct" />
            <asp:Parameter Name="Nov" />
            <asp:Parameter Name="Dec" />
            <asp:Parameter Name="Notes" />            
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        DeleteCommand="DeleteBudgetDetails" 
        DeleteCommandType="StoredProcedure"
        InsertCommand="InsertBudgetDetails"
        InsertCommandType="StoredProcedure"
        SelectCommand="SELECT ID, USERID,Classification,AccountID,AccountCode,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwBudgetDetail where Userid = @USERNAME"
        UpdateCommand="UpdateBudgetDetails"
        UpdateCommandType="StoredProcedure">
        
        <DeleteParameters>
            <asp:Parameter Name="ID" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="AccountID" />
            <asp:Parameter Name="Detail" />
            <asp:Parameter Name="Quantity" />
            <asp:Parameter Name="UnitCost" />
            <asp:Parameter Name="Jan" />
            <asp:Parameter Name="Feb" />
            <asp:Parameter Name="Mar" />
            <asp:Parameter Name="Apr" />
            <asp:Parameter Name="May" />
            <asp:Parameter Name="Jun" />
            <asp:Parameter Name="Jul" />
            <asp:Parameter Name="Aug" />
            <asp:Parameter Name="Sep" />
            <asp:Parameter Name="Oct" />
            <asp:Parameter Name="Nov" />
            <asp:Parameter Name="Dec" />
            <asp:Parameter Name="Year" DefaultValue="2017" />
            <asp:SessionParameter DefaultValue="" Name="USERID" SessionField="USERID" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ACCOUNTID" />
            <asp:Parameter Name="Detail" />
            <asp:Parameter Name="Quantity" />
            <asp:Parameter Name="UnitCost" />
            <asp:Parameter Name="Jan" />
            <asp:Parameter Name="Feb" />
            <asp:Parameter Name="Mar" />
            <asp:Parameter Name="Apr" />
            <asp:Parameter Name="May" />
            <asp:Parameter Name="Jun" />
            <asp:Parameter Name="Jul" />
            <asp:Parameter Name="Aug" />
            <asp:Parameter Name="Sep" />
            <asp:Parameter Name="Oct" />
            <asp:Parameter Name="Nov" />
            <asp:Parameter Name="Dec" />
            <asp:Parameter Name="Year"  DefaultValue="2017"/>
            <asp:Parameter Name="ID" />
            <asp:SessionParameter DefaultValue="" Name="USERID" SessionField="USERID" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceAccount" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="SELECT AccountID, AccountName FROM vwAccountCodes Where Userid = @USERNAME order by AccountName">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, InputUserID,Classification,AccountID,AccountCode,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail where Userid = @USERNAME">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, InputUserID,Classification,AccountID,AccountCode,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
