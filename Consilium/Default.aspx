﻿<%@ Page Title="Consilium" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Consilium._Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

            <script type="text/javascript"> 

                function OnDelete(sender, args) {
                    var lbl = document.getElementById('<%=lblGrid.ClientID%>')
                    if (lbl) {
                        lbl.style.display = ''
                    } 

                }

                

                function OnCommand(sender, args)
                {
                    var grid = $find('<%=RadGrid2.ClientID%>');
                    var totalAmount = 0;
                    if (grid) {
                        if (args.get_commandName() == "Distribute") {
                            var CommandArg = args.get_commandArgument();
                            var rowIndex = Number(CommandArg);

                            var MasterTable = grid.get_masterTableView();

                            var Rows = MasterTable.get_dataItems();
                            var offSet = 0;
                            for (var i = 0; i < Rows.length; i++)
                            {
                                var temprow = Rows[i];  
                                if (temprow._itemIndexHierarchical < 0)
                                {
                                    offSet++;
                                }
                            }
                            if (offSet > 0) {
                                rowIndex = (rowIndex + offSet);
                                if (rowIndex == -1) {
                                    rowIndex = 0;
                                }
                            }
                            var row = Rows[rowIndex];
                            var cell = MasterTable.getCellByColumnUniqueName(row, "ExtendedCost");
                            var ExtendedCost = cell.innerText;
                            var number = Number(ExtendedCost.replace(/[^-?0-9\.]+/g, ""));
                            var distributeamount = number / 12;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });
                            var newItem = MasterTable.get_dataItems()[rowIndex];
                            var batchManager = grid.get_batchEditingManager();
                            var categoryCell = newItem.get_cell("Jan");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Feb");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Mar");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Apr");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("May");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Jun");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Jul");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Aug");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Sep");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Oct");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Nov");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            categoryCell = newItem.get_cell("Dec");
                            batchManager.changeCellValue(categoryCell, distributeamount);

                            args.set_cancel(true);
                        }
                    }
                }

                function ValueChangedRadGrid1(sender, args) {
                    var lbl = document.getElementById('<%=lblGrid.ClientID%>')
                    if (lbl) {
                        lbl.style.display = ''
                    } 

                    var grid = $find('<%=RadGrid1.ClientID%>');
                    var totalAmount = 0;
                    if (grid) {
                        var MasterTable = grid.get_masterTableView();
                        var Rows = MasterTable.get_dataItems();
                        for (var i = 0; i < Rows.length; i++) {
                            var row = Rows[i];

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jan");
                            var Jan = cell.innerText;

                            Jan = Number(Jan.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Feb");
                            var Feb = cell.innerText;

                            Feb = Number(Feb.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Mar");
                            var Mar = cell.innerText;

                            Mar = Number(Mar.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Apr");
                            var Apr = cell.innerText;

                            Apr = Number(Apr.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "May");
                            var May = cell.innerText;

                            May = Number(May.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jun");
                            var Jun = cell.innerText;

                            Jun = Number(Jun.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jul");
                            var Jul = cell.innerText;

                            Jul = Number(Jul.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Aug");
                            var Aug = cell.innerText;

                            Aug = Number(Aug.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Sep");
                            var Sep = cell.innerText;

                            Sep = Number(Sep.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Oct");
                            var Oct = cell.innerText;

                            Oct = Number(Oct.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Nov");
                            var Nov = cell.innerText;

                            Nov = Number(Nov.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Dec");
                            var Dec = cell.innerText;

                            Dec = Number(Dec.replace(/[^-?0-9\.]+/g, ""));

                            Total = Jan + Feb + Mar + Apr + May + Jun + Jul + Aug + Sep + Oct + Nov + Dec;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });

                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "CYTotal");
                            cellTotal.innerText = formatter.format(Total);

                            cell = MasterTable.getCellByColumnUniqueName(row, "CYBudget");
                            var CYBudget = cell.innerText;

                            CYBudget = Number(CYBudget.replace(/[^-?0-9\.]+/g, ""));

                            Total = Total - CYBudget;

                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "Variance");
                            cellTotal.innerText = formatter.format(Total);

                        }

                        //var footerTable = grid.get_masterTableViewFooter().get_element(); 

                        //var masterTableViewFooter = grid.get_masterTableViewFooter();

                        //var footer = MasterTable.get_element().getElementsByTagName("TFOOT")[0];
                        //var col1Footer = footer.rows[0].cells[0].innerHTML; // Get the footer for first column 
                        //footer.rows[0].cells[1].innerHTML = 'My Footer Text'; // Set the footer value for second column 
                        //var col3Footer = footer.rows[0].cells[2].innerHTML; 

                        //alert("masterTableViewFooter = " + col3Footer); 


                        //alert("jimmy");
                        //var columns = MasterTable.get_columns();
                        //alert("after col");
                        //alert("col count = " + columns.length);

                        //// alerts "ID" and "Name"
                        //for (var i = 0; i < columns.length; i++) {
                        //    alert(columns[i].get_uniqueName());
                        //    alert(columns[i].footerCell.innerText);
                        //}

                        //footerCell = grid.footerCell.$(".rgFooter").find("td").get(0);
                        //alert(footerCell.children[0].innerHTML);
                        //alert("jimmy after");

                        //var currentValue = footer.rows[0].cells[7].innerText.replace("$",""); // Get the footer for first column 
                        //alert("col1Footer Text value = " + currentValue);

                        //var currentValue = footer.rows[0].cells[7].innerHTML.substring(footer.rows[0].cells[7].innerHTML.indexOf(':') + 1).trim();
                        //alert("currentValue = " + currentValue);
                        //footerCell.children[0].innerHTML = "Sum : " + (parseFloat(currentValue) - parseFloat((args.get_cellValue()) ? args.get_cellValue().replace(/,/g, "") : 0) +
                        //    parseFloat(args.get_editorValue().replace(/,/g, "")));
                    }
                }


                function ValueChangedRadGrid2(sender, args) {

                    var lbl = document.getElementById('<%=lblGrid.ClientID%>')
                    if (lbl) {
                        lbl.style.display = ''
                    } 

                    var grid = $find('<%=RadGrid2.ClientID%>');
                    var totalAmount = 0;
                    if (grid) {
                        var MasterTable = grid.get_masterTableView();
                        var Rows = MasterTable.get_dataItems();
                        for (var i = 0; i < Rows.length; i++) {
                            var row = Rows[i];
                            var cell = MasterTable.getCellByColumnUniqueName(row, "Quantity");
                            var Quantity = cell.innerText;

                            cell = MasterTable.getCellByColumnUniqueName(row, "UnitCost");
                            var UnitCost = cell.innerText;

                            var number = Number(UnitCost.replace(/[^-?0-9\.]+/g, ""));
                            
                            var Total = Quantity * number;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });
                            
                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "ExtendedCost");
                            cellTotal.innerText = Total;

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jan");
                            var Jan = cell.innerText;

                            Jan = Number(Jan.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Feb");
                            var Feb = cell.innerText;

                            Feb = Number(Feb.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Mar");
                            var Mar = cell.innerText;

                            Mar = Number(Mar.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Apr");
                            var Apr = cell.innerText;

                            Apr = Number(Apr.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "May");
                            var May = cell.innerText;

                            May = Number(May.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jun");
                            var Jun = cell.innerText;

                            Jun = Number(Jun.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Jul");
                            var Jul = cell.innerText;

                            Jul = Number(Jul.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Aug");
                            var Aug = cell.innerText;

                            Aug = Number(Aug.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Sep");
                            var Sep = cell.innerText;

                            Sep = Number(Sep.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Oct");
                            var Oct = cell.innerText;

                            Oct = Number(Oct.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Nov");
                            var Nov = cell.innerText;

                            Nov = Number(Nov.replace(/[^-?0-9\.]+/g, ""));

                            cell = MasterTable.getCellByColumnUniqueName(row, "Dec");
                            var Dec = cell.innerText;

                            Dec = Number(Dec.replace(/[^-?0-9\.]+/g, ""));

                            Total = Jan + Feb + Mar + Apr + May + Jun + Jul + Aug + Sep + Oct + Nov + Dec;

                            var formatter = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });

                            var cellTotal = MasterTable.getCellByColumnUniqueName(row, "Total");
                            cellTotal.innerText = Total;
                            
                        }
                    }
                }

                function ValueChangedRadGrid3(sender, args) {
                    var lbl = document.getElementById('<%=lblGrid.ClientID%>')
                    if (lbl) {
                        lbl.style.display = ''
                    }
                }

            </script>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1"></telerik:RadAjaxLoadingPanel>
    <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecorationZoneID="demo" DecoratedControls="All" EnableRoundedCorners="false" />
    <h2 style="color:#0091ba">Consilium - Budget Entry and Review Application</h2>
    <br />
    <telerik:RadLabel runat="server" ID="lblChangeUser" Visible="true" Text="Select User:  "/>
    <telerik:RadComboBox ID="cmbChangeUser" runat="server" OnSelectedIndexChanged="cmbChangeUser_SelectedIndexChanged" AutoPostBack="true"></telerik:RadComboBox>
   <telerik:RadLabel runat="server" ID="lblChangeYear" Visible="true" Text="Year:  "/>
    <telerik:RadComboBox ID="cmbChangeYear" runat="server" OnSelectedIndexChanged="cmbChangeYear_SelectedIndexChanged" AutoPostBack="true">
        <Items>
            <telerik:RadComboBoxItem Text="2018-2019" />
            <telerik:RadComboBoxItem Text="2017-2018" />
        </Items>
    </telerik:RadComboBox>
    <br />
    <telerik:RadLabel runat="server" ID="lblGrid" Visible="true" Text="Changes have been made to the grid.  You must click save to apply the changes you have made.. " ForeColor="Red"/>
    <br />
    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="3" Skin="Office2007" >
        <Tabs>
            <telerik:RadTab Text="Current Year Forecast" Width="125px"></telerik:RadTab>
            <telerik:RadTab Text="Next Year Budget" Width="120px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="Budget Comparison" Width="160px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="Current Year Budget" Width="120px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="Management Budget Review" Width="225px"></telerik:RadTab>
            <telerik:RadTab Text="Management Budget Comparison" Width="250px"></telerik:RadTab>
            <telerik:RadTab Text="Fin Budget Review" Width="150px"></telerik:RadTab>
            <telerik:RadTab Text="Fin Forecast Review" Width="175px"></telerik:RadTab>
            <telerik:RadTab Text="Fin Budget Comparison" Width="200px"></telerik:RadTab>
            <telerik:RadTab Text="Fin Admin" Width="125px"></telerik:RadTab>
            <telerik:RadTab Text="YTD Account Detail" Width="225px"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="3" CssClass="outerMultiPage">
        <telerik:RadPageView runat="server" ID="RadPageView1">
            <br />
            <asp:Button runat="server" ID="btnSortAccountCode" Text="Sort by Account Code" OnClick="btnSortAccountCode_Click" />
            <br />
            <br />
            <telerik:RadGrid ID="RadGrid1" runat="server" AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="False" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" AllowFilteringByColumn="true" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource1" GridLines="Both" OnPreRender="RadGrid1_PreRender" PageSize="20" Skin="Office2007" ShowFooter="true" OnItemDataBound="RadGrid1_ItemDataBound" OnPageSizeChanged="RadGrid1_PageSizeChanged">
                <GroupingSettings CaseSensitive="false" />
                <ClientSettings>
                    <ClientEvents OnBatchEditCellValueChanged="ValueChangedRadGrid1"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" PageSize="25" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="AccountCodeDisplay" SortOrder="Ascending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Account" UniqueName="AccountCodeDisplay" ReadOnly="true"  FilterControlWidth="90%" FooterStyle-BackColor="White" FooterStyle-BorderColor="Black" FooterText="Totals">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn  DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jan" UniqueName="Jan" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Feb" UniqueName="Feb" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Mar" UniqueName="Mar" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Apr" UniqueName="Apr" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="May" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="May" UniqueName="May" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jun" UniqueName="Jun" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jul" UniqueName="Jul" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Aug" UniqueName="Aug" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Sep" UniqueName="Sep" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Oct" UniqueName="Oct" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Nov" UniqueName="Nov" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Dec" UniqueName="Dec" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="CYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Budget" ItemStyle-HorizontalAlign="Right" NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="CYBudget" UniqueName="CYBudget" ReadOnly="true" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0}) + (({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year TOTAL" ItemStyle-HorizontalAlign="Right" UniqueName="CYTotal" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterStyle-HorizontalAlign="Right"  AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridCalculatedColumn DataFields="CYBudget, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11}) + (({12} == null) ? 0 : {12}) - (({0} == null) ? 0 : {0})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Variance" ItemStyle-HorizontalAlign="Right" UniqueName="Variance" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterStyle-HorizontalAlign="Right"  AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridBoundColumn DataField="Notes" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Notes" UniqueName="Notes" FilterControlWidth="90%" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="1px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  AllowFiltering="false" ItemStyle-Wrap="false" FooterStyle-Wrap="false" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2">
            <br />
            <asp:Button runat="server" ID="btnSortAccountCode2" Text="Sort by Account Code" OnClick="btnSortAccountCode2_Click" />
            <br />
            <br />
            <telerik:RadGrid ID="RadGrid2" runat="server" AllowFilteringByColumn="true"  AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource2" GridLines="Both" OnPreRender="RadGrid2_PreRender" PageSize="20" Skin="Office2007"  ShowFooter="true"  OnItemDataBound="RadGrid2_ItemDataBound" OnPageSizeChanged="RadGrid2_PageSizeChanged">
                <GroupingSettings CaseSensitive="false" />
                <ClientSettings>
                    <ClientEvents OnBatchEditCellValueChanged="ValueChangedRadGrid2" OnRowDeleted="OnDelete" OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False"  PageSize="25" CommandItemDisplay="Top" DataKeyNames="ID" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="AccountCodeDisplay" SortOrder="Ascending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridTemplateColumn DataField="AccountID" AllowFiltering="false" SortExpression="AccountCodeDisplay" FilterControlWidth="90%" AndCurrentFilterValue ="AccountCodeDisplay" DefaultInsertValue="" HeaderStyle-Width="325px" HeaderText="Account" UniqueName="AccountCodeDisplay" AllowSorting="true"  FooterText="Totals"  FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                            <FilterTemplate>

                            </FilterTemplate>
                            <ItemTemplate>
                                <%# Eval("AccountCodeSingleLine") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadDropDownList ID="AccountsIDDropDown" runat="server" DataSourceID="SqlDataSourceAccount" DataTextField="AccountNameSingleLine" DataValueField="AccountID" RenderMode="Lightweight" Skin="Office2007" Width="395px">
                                </telerik:RadDropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Detail" HeaderStyle-Width="125px" HeaderText="Detail" SortExpression="Detail" UniqueName="Detail" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false">
                            <HeaderStyle Width="40px" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="UnitCost" UniqueName="UnitCost" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Double" Expression="(({0} == null) ? 0 : {0})*(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" UniqueName="ExtendedCost" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridButtonColumn CommandName="Distribute" Text="Distribute" ButtonType="PushButton" HeaderText="Distribute" HeaderStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" >
                        </telerik:GridButtonColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" SortExpression="Jan" UniqueName="Jan" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" SortExpression="Feb" UniqueName="Feb" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}"  DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" SortExpression="Mar" UniqueName="Mar" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" SortExpression="Apr" UniqueName="Apr" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="May" ItemStyle-HorizontalAlign="Right" SortExpression="May" UniqueName="May" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}"  DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" SortExpression="Jun" UniqueName="Jun" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}"  DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" SortExpression="Jul" UniqueName="Jul"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}"  DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" SortExpression="Aug" UniqueName="Aug"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}"  DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" SortExpression="Sep" UniqueName="Sep" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}"  DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" SortExpression="Oct" UniqueName="Oct" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" SortExpression="Nov" UniqueName="Nov" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}"  DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" SortExpression="Dec" UniqueName="Dec" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0}) + (({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" UniqueName="Total" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridButtonColumn CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Delete this product?" ConfirmTitle="Delete" HeaderStyle-Width="50px" HeaderText="Delete" Text="Delete" UniqueName="DeleteColumn" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                            <HeaderStyle Width="50px" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="1px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  AllowFiltering="false" ItemStyle-Wrap="false" FooterStyle-Wrap="false" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView3">
            <br />
            <asp:Button runat="server" ID="btnSortAccountCode3" Text="Sort by Account Code" OnClick="btnSortAccountCode3_Click" />
            <br />
            <br />
            <telerik:RadGrid ID="RadGrid3" runat="server" AllowFilteringByColumn="true"  AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource3" GridLines="Both" OnPreRender="RadGrid3_PreRender" PageSize="20" Skin="Office2007" ShowFooter="true"  OnItemDataBound="RadGrid3_ItemDataBound" OnPageSizeChanged="RadGrid3_PageSizeChanged">
                <GroupingSettings CaseSensitive="false" />
                <ClientSettings>
                    <ClientEvents OnBatchEditCellValueChanged="ValueChangedRadGrid3" OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" PageSize="25" CommandItemDisplay="TopAndBottom" DataKeyNames="AccountCode" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="AccountCodeDisplay" SortOrder="Ascending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" FilterControlWidth="90%"  HeaderStyle-Width="125px" HeaderText="Account" SortExpression="AccountCodeDisplay" UniqueName="AccountCodeDisplay" ReadOnly="true" FooterText="Totals" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="CYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Current Year Budget" ItemStyle-HorizontalAlign="Right" SortExpression="CYBudget" UniqueName="CYBudget" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="CYForecast" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Current Year Forecast" ItemStyle-HorizontalAlign="Right" SortExpression="CYForecast" UniqueName="CYForecast" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="CYForecast, CYBudget" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})-(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Forecast - Current Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="CYFCYCBompare"  FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" NumericType="Number" DataField="NYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Next Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="NYBudget" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="CYForecast, NYBudget" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})-(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Forecast - Next Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="CYFNYBYCompare"  FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridBoundColumn DataField="VarianceComment" HeaderStyle-Width="125px" HeaderText="Variance Comment" SortExpression="VarianceComment" UniqueName="VarianceComment" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="1px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  AllowFiltering="false" ItemStyle-Wrap="false" FooterStyle-Wrap="false" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>

        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView4">
            <br />
            <asp:Button runat="server" ID="btnSortAccountCode4" Text="Sort by Account Code" OnClick="btnSortAccountCode4_Click" />
            <br />
            <br />
            <telerik:RadGrid ID="RadGrid4" runat="server" AllowFilteringByColumn="true"  AllowAutomaticDeletes="False" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="False" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="false" DataSourceID="SqlDataSource4" GridLines="Both" PageSize="20" Skin="Office2007" ShowFooter="true"  OnItemDataBound="RadGrid4_ItemDataBound" OnPageSizeChanged="RadGrid4_PageSizeChanged" OnPreRender="RadGrid4_PreRender">
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="False" PageSize="25" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" ShowSaveChangesButton="false" ShowCancelChangesButton="false"/>
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="AccountCodeDisplay" SortOrder="Ascending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay"  FilterControlWidth="90%"  HeaderStyle-Width="200px" HeaderText="Account" SortExpression="AccountCodeDisplay" UniqueName="AccountCodeDisplay" ReadOnly="true" FooterText="Totals" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Detail" HeaderStyle-Width="125px" HeaderText="Detail" SortExpression="Detail" UniqueName="Detail" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false">
                            <HeaderStyle Width="40px" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="UnitCost" UniqueName="UnitCost" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})*(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" UniqueName="ExtendedCost" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jan" UniqueName="Jan" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Feb" UniqueName="Feb" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Mar" UniqueName="Mar" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Apr" UniqueName="Apr" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="May" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="May" UniqueName="May" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jun" UniqueName="Jun" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jul" UniqueName="Jul" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Aug" UniqueName="Aug"  ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Sep" UniqueName="Sep" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Oct" UniqueName="Oct" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Nov" UniqueName="Nov" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Dec" UniqueName="Dec"  ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0}) + (({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" UniqueName="Total" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="1px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  AllowFiltering="false" ItemStyle-Wrap="false" FooterStyle-Wrap="false" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView5">
            <telerik:RadGrid ID="RadGrid5" runat="server"  AllowAutomaticDeletes="False" AllowSorting="true" AllowAutomaticInserts="False" AllowAutomaticUpdates="False" AllowPaging="True" AllowFilteringByColumn="true" IsFilterItemExpanded="false"  AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource5" GridLines="Both" PageSize="20" Skin="Office2007" ShowFooter="true"  OnItemDataBound="RadGrid5_ItemDataBound" OnItemCommand="RadGrid5_ItemCommand" OnPageSizeChanged="RadGrid5_PageSizeChanged" OnPreRender="RadGrid5_PreRender">
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="False" PageSize="25" CommandItemDisplay="TopAndBottom" DataKeyNames="ID"  EditMode="Batch" HorizontalAlign="NotSet">
                    <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" ShowSaveChangesButton="false" ShowCancelChangesButton="false" ShowExportToExcelButton="true"/>
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="InputUserID" SortOrder="Ascending" />
                    </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="InputUserID" DefaultInsertValue="" HeaderStyle-Width="100px" HeaderText="User Name" UniqueName="InputUserID" ReadOnly="true" FilterControlWidth="80%" FooterText="Totals" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay" DefaultInsertValue="" HeaderStyle-Width="275px" HeaderText="Account Code" UniqueName="AccountCodeDisplay" ReadOnly="true"  FilterControlWidth="90%" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Detail" DefaultInsertValue="" HeaderStyle-Width="125px" HeaderText="Detail" UniqueName="Detail" ReadOnly="true" FilterControlWidth="80%" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity" ReadOnly="true" AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="UnitCost" UniqueName="UnitCost" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})*(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" UniqueName="ExtendedCost"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jan" UniqueName="Jan" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Feb" UniqueName="Feb" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Mar" UniqueName="Mar" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Apr" UniqueName="Apr" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="May" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="May" UniqueName="May" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jun" UniqueName="Jun" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jul" UniqueName="Jul" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Aug" UniqueName="Aug" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Sep" UniqueName="Sep" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Oct" UniqueName="Oct" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Nov" UniqueName="Nov" ReadOnly="true" AllowFiltering="false"  FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Dec" UniqueName="Dec" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0}) + (({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" UniqueName="Total"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridCalculatedColumn>
                     </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView6">
            <telerik:RadGrid ID="RadGrid6" runat="server" AllowFilteringByColumn="true"  AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource6" GridLines="Both" PageSize="20" Skin="Office2007" ShowFooter="true"  OnItemDataBound="RadGrid6_ItemDataBound" OnItemCommand="RadGrid6_ItemCommand" OnPageSizeChanged="RadGrid6_PageSizeChanged" OnPreRender="RadGrid6_PreRender">
                <GroupingSettings CaseSensitive="false" />
                <ClientSettings>
                    <ClientEvents OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" PageSize="25" CommandItemDisplay="TopAndBottom" DataKeyNames="AccountCode" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false"  ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowExportToExcelButton="true"/>
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="EmpName" SortOrder="Ascending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="EmpName"  FilterControlWidth="85%"  HeaderStyle-Width="125px" HeaderText="User Name" SortExpression="EmpName" UniqueName="EmpName" ReadOnly="true" FooterText="Totals" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay" FilterControlWidth="90%"  HeaderStyle-Width="125px" HeaderText="Account Code" SortExpression="AccountCodeDisplay" UniqueName="AccountCodeDisplay" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="CYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Current Year Budget" ItemStyle-HorizontalAlign="Right" SortExpression="CYBudget" UniqueName="CYBudget" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="CYForecast" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Current Year Forecast" ItemStyle-HorizontalAlign="Right" SortExpression="CYForecast" UniqueName="CYForecast" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="CYForecast, CYBudget" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})-(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Forecast - Current Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="CYFCYCBompare" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="NYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Next Year Budget" ItemStyle-HorizontalAlign="Right" SortExpression="NYBudget" UniqueName="NYBudget" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="CYForecast, NYBudget" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})-(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Forecast - Next Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="CYFNYBYCompare" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridBoundColumn DataField="VarianceComment" HeaderStyle-Width="125px" HeaderText="Variance Comment" SortExpression="VarianceComment" UniqueName="VarianceComment" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView7">
            <telerik:RadGrid ID="RadGrid7" runat="server" PageSize="25" AllowAutomaticDeletes="False" AllowSorting="true" AllowAutomaticInserts="False" AllowAutomaticUpdates="False" AllowPaging="True" AllowFilteringByColumn="true" IsFilterItemExpanded="false"  AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource7" GridLines="Both" Skin="Office2007"  ShowFooter="true"  OnItemDataBound="RadGrid7_ItemDataBound" OnItemCommand="RadGrid7_ItemCommand" OnPageSizeChanged="RadGrid7_PageSizeChanged" OnPreRender="RadGrid7_PreRender">
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" EditMode="Batch" HorizontalAlign="NotSet">
                    <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" ShowSaveChangesButton="false" ShowCancelChangesButton="false" ShowExportToExcelButton="true"/>
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="InputUserID" SortOrder="Ascending" />
                    </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="InputUserID" DefaultInsertValue="" HeaderStyle-Width="100px" HeaderText="User Name" UniqueName="InputUserID" ReadOnly="true" FilterControlWidth="85%"  FooterText="Totals" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay" DefaultInsertValue="" HeaderStyle-Width="275px" HeaderText="Account" UniqueName="AccountCodeDisplay" ReadOnly="true"  FilterControlWidth="90%" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Detail" DefaultInsertValue="" HeaderStyle-Width="130px" HeaderText="Detail" UniqueName="Detail" ReadOnly="true"  FilterControlWidth="80%" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" SortExpression="Quantity" UniqueName="Quantity" ReadOnly="true" AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="UnitCost" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="UnitCost" UniqueName="UnitCost" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Quantity, UnitCost" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})*(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Ext Cost" ItemStyle-HorizontalAlign="Right" UniqueName="ExtendedCost"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jan" UniqueName="Jan" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Feb" UniqueName="Feb" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Mar" UniqueName="Mar" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Apr" UniqueName="Apr" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="May" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="May" UniqueName="May" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jun" UniqueName="Jun" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jul" UniqueName="Jul" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Aug" UniqueName="Aug" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Sep" UniqueName="Sep" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Oct" UniqueName="Oct" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Nov" UniqueName="Nov" ReadOnly="true" AllowFiltering="false"  FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Dec" UniqueName="Dec" ReadOnly="true"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0}) + (({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Total" ItemStyle-HorizontalAlign="Right" UniqueName="Total"  AllowFiltering="false" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right">
                        </telerik:GridCalculatedColumn>
                     </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView8">
            <telerik:RadGrid ID="RadGrid8" PageSize="25" runat="server" AllowFilteringByColumn="true" AllowAutomaticDeletes="False" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="False" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource8" GridLines="Both" Skin="Office2007"  ShowFooter="true"  OnItemDataBound="RadGrid8_ItemDataBound" OnItemCommand="RadGrid8_ItemCommand" OnPageSizeChanged="RadGrid8_PageSizeChanged" OnPreRender="RadGrid8_PreRender">
                <GroupingSettings CaseSensitive="false" />
                <ClientSettings>
                    <ClientEvents OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView  AutoGenerateColumns="False" CommandItemDisplay="TopAndBottom" DataKeyNames="ID" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="true" ShowAddNewRecordButton="false" ShowSaveChangesButton="false" ShowCancelChangesButton="false"/>
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="UserName" SortOrder="Ascending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="UserName" DefaultInsertValue="" HeaderStyle-Width="125px" HeaderText="User Name" UniqueName="UserName" ReadOnly="true"  FilterControlWidth="85%"  FooterText="Totals" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Account" UniqueName="AccountCodeDisplay" ReadOnly="true"  FilterControlWidth="90%" FooterStyle-BackColor="White" FooterStyle-BorderColor="Black" FooterText="Totals">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn  DataField="Jan" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jan" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jan" UniqueName="Jan" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Feb" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Feb" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Feb" UniqueName="Feb" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Mar" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Mar" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Mar" UniqueName="Mar" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Apr" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Apr" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Apr" UniqueName="Apr" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="May" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="May" UniqueName="May" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jun" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jun" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jun" UniqueName="Jun" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Jul" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Jul" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Jul" UniqueName="Jul" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Aug" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Aug" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Aug" UniqueName="Aug" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Sep" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Sep" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Sep" UniqueName="Sep" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Oct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Oct" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Oct" UniqueName="Oct" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Nov" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Nov" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Nov" UniqueName="Nov" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Dec" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="60px" HeaderText="Dec" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="Dec" UniqueName="Dec" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterStyle-HorizontalAlign="Right" AllowFiltering="false" ReadOnly="true">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="CYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Budget" ItemStyle-HorizontalAlign="Right" NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" SortExpression="CYBudget" UniqueName="CYBudget" ReadOnly="true" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0}) + (({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year TOTAL" ItemStyle-HorizontalAlign="Right" UniqueName="CYTotal" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterStyle-HorizontalAlign="Right"  AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridCalculatedColumn DataFields="CYBudget, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ,Dec" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({1} == null) ? 0 : {1}) + (({2} == null) ? 0 : {2}) + (({3} == null) ? 0 : {3}) + (({4} == null) ? 0 : {4}) + (({5} == null) ? 0 : {5}) +(({6} == null) ? 0 : {6}) + (({7} == null) ? 0 : {7}) + (({8} == null) ? 0 : {8}) + (({9} == null) ? 0 : {9}) + (({10} == null) ? 0 : {10}) + (({11} == null) ? 0 : {11}) + (({12} == null) ? 0 : {12}) - (({0} == null) ? 0 : {0})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Variance" ItemStyle-HorizontalAlign="Right" UniqueName="Variance" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterStyle-HorizontalAlign="Right"  AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridBoundColumn DataField="Notes" DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Notes" UniqueName="Notes" FilterControlWidth="90%" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="1px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  AllowFiltering="false" ItemStyle-Wrap="false" FooterStyle-Wrap="false" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView9">
            <telerik:RadGrid ID="RadGrid9" PageSize="25" runat="server" AllowFilteringByColumn="true"  AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource9" GridLines="Both" Skin="Office2007" ShowFooter="true"  OnItemDataBound="RadGrid9_ItemDataBound" OnItemCommand="RadGrid9_ItemCommand" OnPageSizeChanged="RadGrid9_PageSizeChanged" OnPreRender="RadGrid9_PreRender">
                <GroupingSettings CaseSensitive="false" />
                <ClientSettings>
                    <ClientEvents OnCommand="OnCommand"/>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False"  CommandItemDisplay="TopAndBottom" DataKeyNames="AccountCode" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                    <BatchEditingSettings EditType="Cell" />
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="AccountCodeDisplay" SortOrder="Ascending" />
                   </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountCodeDisplay" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" FilterControlWidth="90%"  HeaderStyle-Width="125px" HeaderText="Account" SortExpression="AccountCodeDisplay" UniqueName="AccountCodeDisplay" ReadOnly="true" FooterText="Totals" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="CYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Current Year Budget" ItemStyle-HorizontalAlign="Right" SortExpression="CYBudget" UniqueName="CYBudget" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn NumericType="Number" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataField="CYForecast" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Current Year Forecast" ItemStyle-HorizontalAlign="Right" SortExpression="CYForecast" UniqueName="CYForecast" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="CYForecast, CYBudget" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})-(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Forecast - Current Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="CYFCYCBompare"  FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridNumericColumn DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" NumericType="Number" DataField="NYBudget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px" HeaderText="Next Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="NYBudget" ReadOnly="true" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridNumericColumn>
                        <telerik:GridCalculatedColumn DataFields="CYForecast, NYBudget" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" DataType="System.Decimal" Expression="(({0} == null) ? 0 : {0})-(({1} == null) ? 0 : {1})" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="80px" HeaderText="Current Year Forecast - Next Year Budget" ItemStyle-HorizontalAlign="Right" UniqueName="CYFNYBYCompare"  FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black"  FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum"  FooterStyle-HorizontalAlign="Right" AllowFiltering="false">
                        </telerik:GridCalculatedColumn>
                        <telerik:GridBoundColumn DataField="VarianceComment" HeaderStyle-Width="125px" HeaderText="Variance Comment" SortExpression="VarianceComment" UniqueName="VarianceComment" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black" AllowFiltering="false"  ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountCode" DefaultInsertValue="" HeaderStyle-Width="1px" HeaderText="Account Code" UniqueName="AccountCode" ReadOnly="true"  AllowFiltering="false" ItemStyle-Wrap="false" FooterStyle-Wrap="false" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings AllowKeyboardNavigation="true">
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView10">
            <br />
            <asp:Label ID="ConvertID" runat="server" Text="Consilium State" Font-Size="XX-Large" ForeColor="#0091BA"></asp:Label>
            <br />
            <table id="GrantTable" style="width:100%;">
                <tr>
                    <td style="width: 256px;padding-left:50px">
                        <asp:Label ID="lblLockConsilium" runat="server" Text="Lock Down Consilium" Font-Size="Large"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadCheckBox runat="server" ID="chkLockDown" OnCheckedChanged="chkLockDown_CheckedChanged"></telerik:RadCheckBox>
                    </td>
                </tr>
            </table>
            <br />

            <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip2"  MultiPageID="RadMultiPage2" SelectedIndex="3" Skin="Office2007" >
                <Tabs>
                    <telerik:RadTab Text="Account Settings" Width="175px" Visible="false"></telerik:RadTab>
                    <telerik:RadTab Text="Budget Export" Width="150px" Selected="True"></telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>

            <telerik:RadMultiPage runat="server" ID="RadMultiPage2"  SelectedIndex="0" CssClass="outerMultiPage">
                <telerik:RadPageView runat="server" ID="RadPageView11">

                    <telerik:RadGrid ID="RadGrid10" runat="server" AllowFilteringByColumn="true"  AllowAutomaticDeletes="True" AllowSorting="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="False" DataSourceID="SqlDataSource10" GridLines="Both" PageSize="20" Skin="Office2007" OnPreRender="RadGrid10_PreRender">
                        <GroupingSettings CaseSensitive="false" />
                        <ClientSettings>
                            <ClientEvents OnCommand="OnCommand"/>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" PageSize="100" CommandItemDisplay="TopAndBottom" DataKeyNames="AccountID" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                            <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="true" />
                            <BatchEditingSettings EditType="Cell" />
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="AccountID" SortOrder="Descending" />
                           </SortExpressions>
                            <Columns>
                                <telerik:GridBoundColumn DataField="AccountCode" FilterControlWidth="100%" HeaderText="Account Code">
                                     <HeaderStyle Width="325px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Description" FilterControlWidth="90%" HeaderStyle-Width="40%" HeaderText="Description" SortExpression="Description" UniqueName="Description">
                                    <HeaderStyle Width="325Px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AccountCodeDisplay" FilterControlWidth="90%" HeaderStyle-Width="40%" HeaderText="Display Text" SortExpression="AccountCodeDisplay" UniqueName="AccountCodeDisplay" ReadOnly="true">
                                    <HeaderStyle Width="325px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="AssignedUser" SortExpression="AssignedUser"  FilterControlWidth="90%"  DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Assigned User" UniqueName="AssignedUser" AllowSorting="true" AllowFiltering="false">
                                    <ItemTemplate>
                                        <%# Eval("AssignedUser") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadDropDownList ID="AssignedIDDropDown" runat="server" DataSourceID="SqlDataSourceReview" DataTextField="UserID" DataValueField="ID" RenderMode="Lightweight" Skin="Office2007" Width="200px">
                                        </telerik:RadDropDownList>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Reviewer" SortExpression="Reviewer"  FilterControlWidth="90%"  DefaultInsertValue="" HeaderStyle-Width="200px" HeaderText="Reviewer" UniqueName="AccountName" AllowSorting="true"  AllowFiltering="false">
                                    <ItemTemplate>
                                        <%# Eval("Reviewer") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadDropDownList ID="ReviewerIDDropDown" runat="server" DataSourceID="SqlDataSourceReview" DataTextField="UserID" DataValueField="ID" RenderMode="Lightweight" Skin="Office2007" Width="200px">
                                        </telerik:RadDropDownList>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Classification" SortExpression="Classification"  FilterControlWidth="90%"  DefaultInsertValue="" HeaderStyle-Width="350px" HeaderText="Classification" UniqueName="Classification" AllowSorting="true"  AllowFiltering="false">
                                    <ItemTemplate>
                                        <%# Eval("Classification") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadDropDownList ID="ClassificationIDDropDown" runat="server" DataSourceID="SqlDataSourceClassification" DataTextField="Classification" DataValueField="ClassificationID" RenderMode="Lightweight" Skin="Office2007" Width="110%">
                                        </telerik:RadDropDownList>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="AccountID"  HeaderStyle-Width="1px" HeaderText="Account ID" SortExpression="AccountID" UniqueName="AccountID" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Delete this product?" ConfirmTitle="Delete" HeaderStyle-Width="50px" HeaderText="Delete" Text="Delete" UniqueName="DeleteColumn" FooterStyle-BackColor="White"  FooterStyle-BorderColor="Black">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                        <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                        <ClientSettings AllowKeyboardNavigation="true">
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:RadPageView>
                <telerik:RadPageView runat="server" ID="RadPageView12">
                    <telerik:RadGrid runat="server"  ID="RadGrid11" DataSourceID="SqlDataSource11" Skin="Office2007" OnItemCommand="RadGrid11_ItemCommand" >
                        <GroupingSettings CaseSensitive="false" />
                        <MasterTableView AutoGenerateColumns="True" PageSize="100" CommandItemDisplay="TopAndBottom" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                            <CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false"  ShowExportToExcelButton="true"/>
                        </MasterTableView>
                    </telerik:RadGrid>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView13">
            <telerik:RadGrid runat="server"  ID="RadGrid12" DataSourceID="SqlDataSource12" Skin="Office2007" AllowFilteringByColumn="false">
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="False" PageSize="100" CommandItemDisplay="TopAndBottom" HorizontalAlign="NotSet" AllowMultiColumnSorting="False" AllowSorting="true">
                    <CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false"  ShowExportToExcelButton="true"/>
                    <Columns>
                        <telerik:GridBoundColumn DataField="AccountDescription" HeaderText="Account Name" FilterControlWidth="90%"  >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Account"  HeaderText="Account Code" FilterControlWidth="90%" HeaderStyle-Width="250px" >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Vendor" HeaderText="Vendor" FilterControlWidth="90%"  >
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn HeaderText="Date" DataField="Date" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:M/d/yyyy}" SortExpression="ProjectStartDate" UniqueName="Date" FilterControlWidth="90%" HeaderStyle-Width="100px" >
                        </telerik:GridDateTimeColumn>
                        <telerik:GridNumericColumn DataField="Amount" DataFormatString="{0:$#,##0.#0;-$#,##0.#0}" HeaderStyle-HorizontalAlign="Center" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" NumericType="Number" SortExpression="Amount" UniqueName="Amount" ReadOnly="true"  AllowFiltering="false" FooterAggregateFormatString="{0:$#,##0.#0;-$#,##0.#0}" Aggregate="Sum">
                        </telerik:GridNumericColumn>
                        <telerik:GridBoundColumn DataField="Description" FilterControlWidth="90%"  HeaderText="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="InvoiceNumber" HeaderText="Invoice #" FilterControlWidth="90%"  >
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, CYBudget, USERID,AccountCode,AccountCodeDisplay,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Notes FROM vwForecastDetail where Userid = @USERNAME and ID is not null and Year = @YEAR order by AccountCodeDisplay"
        UpdateCommand="UpdateForecastDetails"
        UpdateCommandType="StoredProcedure" OnUpdating="SqlDataSource1_Updating">
        <UpdateParameters>
            <asp:Parameter Name="ID" />
            <asp:Parameter Name="Jan" />    
            <asp:Parameter Name="Feb" />
            <asp:Parameter Name="Mar" />
            <asp:Parameter Name="Apr" />
            <asp:Parameter Name="May" />
            <asp:Parameter Name="Jun" />
            <asp:Parameter Name="Jul" />
            <asp:Parameter Name="Aug" />
            <asp:Parameter Name="Sep" />
            <asp:Parameter Name="Oct" />
            <asp:Parameter Name="Nov" />
            <asp:Parameter Name="Dec" />
            <asp:Parameter Name="Notes" />        
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        DeleteCommand="DeleteBudgetDetails" 
        DeleteCommandType="StoredProcedure"
        InsertCommand="InsertBudgetDetails"
        InsertCommandType="StoredProcedure"
        SelectCommand="SELECT ID, USERID,Classification,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwBudgetDetail where Userid = @USERNAME and Year = @year + 1 order by AccountCodeDisplay"
        UpdateCommand="UpdateBudgetDetails"
        UpdateCommandType="StoredProcedure" OnUpdating="SqlDataSource2_Updating" OnInserting="SqlDataSource2_Inserting">
        
        <DeleteParameters>
            <asp:Parameter Name="ID" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="AccountID" />
            <asp:Parameter Name="Detail" />
            <asp:Parameter Name="Quantity" />
            <asp:Parameter Name="UnitCost" />
            <asp:Parameter Name="Jan" />
            <asp:Parameter Name="Feb" />
            <asp:Parameter Name="Mar" />
            <asp:Parameter Name="Apr" />
            <asp:Parameter Name="May" />
            <asp:Parameter Name="Jun" />
            <asp:Parameter Name="Jul" />
            <asp:Parameter Name="Aug" />
            <asp:Parameter Name="Sep" />
            <asp:Parameter Name="Oct" />
            <asp:Parameter Name="Nov" />
            <asp:Parameter Name="Dec" />
            <asp:SessionParameter DefaultValue="" Name="USERID" SessionField="USERID" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="AccountCodeDisplay" />
            <asp:Parameter Name="Detail" />
            <asp:Parameter Name="Quantity" />
            <asp:Parameter Name="UnitCost" />
            <asp:Parameter Name="Jan" />
            <asp:Parameter Name="Feb" />
            <asp:Parameter Name="Mar" />
            <asp:Parameter Name="Apr" />
            <asp:Parameter Name="May" />
            <asp:Parameter Name="Jun" />
            <asp:Parameter Name="Jul" />
            <asp:Parameter Name="Aug" />
            <asp:Parameter Name="Sep" />
            <asp:Parameter Name="Oct" />
            <asp:Parameter Name="Nov" />
            <asp:Parameter Name="Dec" />
            <asp:Parameter Name="Year"  DefaultValue="2017"/>
            <asp:Parameter Name="ID" />
            <asp:SessionParameter DefaultValue="" Name="USERID" SessionField="USERID" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceAccount" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="SELECT AccountID, AccountName, AccountNameSingleLine FROM vwAccountCodes Where Userid = @USERNAME and Year = @Year order by AccountName ">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT USERID,AccountCode,AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonDetail where Userid = @USERNAME and Year = @Year order by AccountCodeDisplay"
        UpdateCommand="UpdateComparisonDetails"
        UpdateCommandType="StoredProcedure" OnUpdating="SqlDataSource3_Updating">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="AccountCode" />
            <asp:Parameter Name="VarianceComment" />
            <asp:SessionParameter Name="USERID" SessionField="USERID" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="Year" SessionField="YEAR" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, USERID,Classification,AccountID,AccountCodeDisplay, AccountCode,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwCYBudgetDetail where Userid = @USERNAME and Year = @Year order by AccountCodeDisplay">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, InputUserID,USERID,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail where Userid = @USERNAME and Year = @year + 1 order by AccountCodeDisplay">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT USERID,EmpName,AccountCode, AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonReviewDetail where UserID = @USERNAME and Year = @Year order by EmpName">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, InputUserID,AccountID,AccountCode,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Detail FROM vwReviewDetail  where Year = @Year+1">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
       <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ID, CYBudget, USERID, UserName,AccountCode,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Notes FROM vwForecastDetail where year = @Year">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource> 
    <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT USERID,EmpName,AccountCode,AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonDetail where Year = @Year">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="YEAR" SessionField="YEAR" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource10" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select AccountID, AccountCode, Description, AssignedUser, ClassificationID,  Classification, Reviewer, AccountCodeDisplay from vwAccountUsers" 
        InsertCommand="InsertAccountDetails"
        InsertCommandType="StoredProcedure"
        UpdateCommand="UpdateAccountDetails"
        UpdateCommandType="StoredProcedure"
        DeleteCommand="DeleteAccountDetails" 
        DeleteCommandType="StoredProcedure">
        <UpdateParameters>
            <asp:Parameter Name="AccountID" />
            <asp:Parameter Name="AccountCode" />    
            <asp:Parameter Name="Description" />
            <asp:Parameter Name="Classification" />
            <asp:Parameter Name="AssignedUser" />
            <asp:Parameter Name="Reviewer" />
        </UpdateParameters> 
        <InsertParameters>
            <asp:Parameter Name="AccountCode" />    
            <asp:Parameter Name="Description" />
            <asp:Parameter Name="Classification" />
            <asp:Parameter Name="AssignedUser" />
            <asp:Parameter Name="Reviewer" />
        </InsertParameters>
        <DeleteParameters>
            <asp:Parameter Name="AccountID" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource11" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT AccountCodeRaw,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource12" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT * FROM vwYTDActualLedger where Userid = @USERNAME order by date">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USERNAME" SessionField="USERNAME" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    
    <asp:SqlDataSource ID="SqlDataSourceReview" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="Select ID, USERID from users where Active = 1">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceClassification" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="Select ID as ClassificationID, Classification from Classifications">
    </asp:SqlDataSource>


</asp:Content>
