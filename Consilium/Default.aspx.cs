﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using System.Collections;
using System.Web;

using Telerik.Web.UI;

namespace Consilium
{
    public partial class _Default : Page
    {
        string myConnectionString = string.Empty;

        static string m_strUserName = "";
        static string m_strUserID = "";
        static string m_strYear = "";

        static string m_strCurrentYearForecastText = "Current Year Forecast";
        static string m_strNextYearBudget = "Next Year Budget";
        static string m_strBudgetComparison = "Budget Comparison";
        static string m_strCurrentYearBudget = "Current Year Budget";
        static string m_strManagementBudgetReview = "Management Budget Review";
        static string m_strManagementBudgetComparison = "Management Budget Comparison";
        static string m_strFinanceBudgetReview = "Fin Budget Review";
        static string m_strFinanceForecastReview = "Fin Forecast Review";
        static string m_strFinanceBudgetComparison = "Fin Budget Comparison";
        static string m_strFinanceAdmin = "Fin Admin";

        protected void Page_Load(object sender, EventArgs e)
        {

            //Needed for speed
            ClearSQLSelectStatements();

            //Response.Redirect("~/Maintenance.aspx");
            if (!IsPostBack)
            {
                m_strCurrentYearForecastText = "Current Year Forecast";
                m_strNextYearBudget = "Next Year Budget";

                m_strYear = "2018";
                Session["YEAR"] = m_strYear;
                LoadCurrentUser(true);
                if (string.IsNullOrEmpty(m_strUserID))
                {
                    return;
                }

                SetAdminMode();


            }else
            {
                LoadCurrentUser(false);
                SetSelectStatements();
            }

            RadGrid1.MasterTableView.AllowAutomaticInserts = true;
        }

        private void ClearSQLSelectStatements()
        {
            SqlDataSource1.SelectCommand = "";
            SqlDataSource2.SelectCommand = "";
            SqlDataSource3.SelectCommand = "";
            SqlDataSource4.SelectCommand = "";
            SqlDataSource5.SelectCommand = "";
            SqlDataSource6.SelectCommand = "";
            SqlDataSource7.SelectCommand = "";
            SqlDataSource8.SelectCommand = "";
            SqlDataSource9.SelectCommand = "";
            SqlDataSource10.SelectCommand = "";
            SqlDataSource11.SelectCommand = "";
            SqlDataSource12.SelectCommand = "";
        }

        private string GetUserEmail(string strToken)
        {
            string selectSQL = "SELECT Email from Users where Token = '" + strToken + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;



            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strUserEmail = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strUserEmail = reader["Email"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            return strUserEmail;
        }


        //private string GetLoggedInUser()
        //{
        //    string strToken = Request.QueryString["TOKEN"];
        //    string strLoggedInUserID = GetUserEmail(strToken);

        //    if (string.IsNullOrEmpty(strLoggedInUserID))
        //    {
        //        Response.Redirect("~\\About.aspx");
        //    }


        //    ////Need to check for delegate redirect
        //    //string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    //string selectSQL = "SELECT DelegateEmail from Users where Email = '" + strLoggedInUserID + "'";

        //    //SqlConnection conn = new SqlConnection(connString);
        //    //SqlCommand cmd = new SqlCommand(selectSQL, conn);
        //    //SqlDataReader reader;

        //    //string strDelegateEmail = string.Empty;

        //    //try
        //    //{
        //    //    conn.Open();
        //    //    reader = cmd.ExecuteReader();
        //    //    while (reader.Read())
        //    //    {
        //    //        strDelegateEmail = reader["DelegateEmail"].ToString();
        //    //    }
        //    //    reader.Close();
        //    //}
        //    //catch (Exception err)
        //    //{
        //    //    //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
        //    //    Console.Write(err);
        //    //}
        //    //finally
        //    //{
        //    //    conn.Close();
        //    //}

        //    //if (!string.IsNullOrEmpty(strDelegateEmail))
        //    //{
        //    //    strLoggedInUserID = strDelegateEmail;
        //    //}

        //    //if (strLoggedInUserID.ToLower() == "avaughan@stdavidsfoundation.org")
        //    //{
        //    //    Session["ADMINMODE"] = true;
        //    //}

        //    return strLoggedInUserID;

        //}

        private string GetLoggedInUser()
        {
            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;

            //Debug Only
            //strLoggedInUserID = "jimmy.voegele@gmail.com";

            if (string.IsNullOrEmpty(strLoggedInUserID) || strLoggedInUserID.ToUpper().Contains("SDFBUILD"))
            {
                strLoggedInUserID = "jimmy.voegele@gmail.com";
            }

            //Need to check for delegate redirect
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string selectSQL = "SELECT DelegateEmail from Users where Email = '" + strLoggedInUserID + "'";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strDelegateEmail = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strDelegateEmail = reader["DelegateEmail"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            if (!string.IsNullOrEmpty(strDelegateEmail))
            {
                strLoggedInUserID = strDelegateEmail;
            }

            if (strLoggedInUserID.ToLower() == "avaughan@stdavidsfoundation.org")
            {
                Session["ADMINMODE"] = true;
            }

            return strLoggedInUserID;

        }
    

        private void SetAdminMode()
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string selectSQL = "SELECT ConsiliumLocked from AdminSettings";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strConsiliumLocked = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strConsiliumLocked = reader["ConsiliumLocked"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            if (strConsiliumLocked == "False")
            {
                Session["CONSILIUMLOCKED"] = false;
            }else
            {
                Session["CONSILIUMLOCKED"] = true;
                chkLockDown.Checked = true;
            }

        }

        protected void LoadCurrentUser(bool bReloadTabs)
        {
            string strLoggedInUserID = GetLoggedInUser();

            if (!string.IsNullOrEmpty(strLoggedInUserID))
            {

                string CurrentUserName = GetUser(strLoggedInUserID);

                m_strUserName = CurrentUserName;

                Session["USERNAME"] = m_strUserName;

                string strUserID = GetUserID(CurrentUserName);
                m_strUserID = strUserID;
                Session["USERID"] = m_strUserID;

                if (!string.IsNullOrEmpty(CurrentUserName))
                {
                    if (bReloadTabs)
                        SetTabVisbility(CurrentUserName);
                }
                else
                    Response.Redirect("~\\About.aspx");
            }
        }

        private string GetUser(string strLoggedInUser)
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string selectSQL = "SELECT UserID from Users where Email = '" + strLoggedInUser + "'";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strUserID = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strUserID = reader["UserID"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            return strUserID;
        }

        private void LoadUsers()
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string selectSQL = "SELECT UserID, FirstName, LastName from Users where Email <> 'AVaughan@stdavidsfoundation.org' and Active = 1 order by LastName";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            cmbChangeUser.Items.Clear();
            RadComboBoxItem rcb = new RadComboBoxItem("Amy Vaughan", "avaughan");
            cmbChangeUser.Items.Add(rcb);


            string strUserID = string.Empty;
            string strFirstName = string.Empty;
            string strLastName = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strUserID = reader["UserID"].ToString();
                    strFirstName = reader["FirstName"].ToString();
                    strLastName = reader["LastName"].ToString();

                    rcb = new RadComboBoxItem(strFirstName + " " + strLastName, strUserID);
                    cmbChangeUser.Items.Add(rcb);

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        private string GetUserID(string UserName)
        {
            string selectSQL = "SELECT ID from Users where UserID = '" + UserName + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strUserID = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strUserID = reader["ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            return strUserID;
        }

        private void SetSelectStatements()
        {

            bool m_bUser = false;
            bool m_bReviewer = false;
            bool m_bFinance = false;

            string selectSQL = "SELECT RoleName from vwUserRoles where UserID = '" + m_strUserName + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strRole = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strRole = reader["RoleName"].ToString();
                    switch (strRole.ToUpper())
                    {
                        case "USER":
                            m_bUser = true;
                            break;
                        case "REVIEWER":
                            m_bReviewer = true;
                            break;
                        case "FINANCE":
                            m_bFinance = true;
                            break;
                    }
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            int nYear = Convert.ToInt32(m_strYear);
            int nYearPlus1 = Convert.ToInt32(m_strYear) + 1;
            if (m_bUser)
            {
                SqlDataSource1.SelectCommand = "SELECT ID, CYBudget, USERID,AccountCode,AccountCodeDisplay,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Notes FROM vwForecastDetail where Userid = '" + m_strUserName + "' and ID is not null and Year = " + nYear + " order by AccountCodeDisplay";
                SqlDataSource2.SelectCommand = "SELECT ID, USERID,Classification,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwBudgetDetail where Userid =  '" + m_strUserName + "' and Year =  " + nYearPlus1 + " order by AccountCodeDisplay";
                SqlDataSource3.SelectCommand = "SELECT USERID,AccountCode,AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonDetail where Userid = '" + m_strUserName + "' and Year =  " + nYear + " order by AccountCodeDisplay";
                SqlDataSource4.SelectCommand = "SELECT ID, USERID,Classification,AccountID,AccountCodeDisplay, AccountCode,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwCYBudgetDetail where Userid = '" + m_strUserName + "' and Year = " + nYear + "  order by AccountCodeDisplay";
                SqlDataSource12.SelectCommand = "SELECT * FROM vwYTDActualLedger where Userid = '" + m_strUserName + "' order by date";
            }else
            {
                SqlDataSource1.SelectCommand = "";
                SqlDataSource2.SelectCommand = "";
                SqlDataSource3.SelectCommand = "";
                SqlDataSource4.SelectCommand = "";
                SqlDataSource12.SelectCommand = "";
            }

            if (m_bReviewer)
            {
                SqlDataSource5.SelectCommand = "SELECT ID, InputUserID,USERID,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail where Userid = '" + m_strUserName + "' and Year =  " + nYearPlus1 + " order by AccountCodeDisplay";
                SqlDataSource6.SelectCommand = "SELECT USERID,EmpName,AccountCode, AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonReviewDetail where UserID = '" + m_strUserName + "' and Year =  " + nYear + " order by EmpName";
            }
            else
            {
                SqlDataSource5.SelectCommand = "";
                SqlDataSource6.SelectCommand = "";
            }

            if (m_bFinance)
            {
                SqlDataSource7.SelectCommand = "SELECT ID, InputUserID,USERID,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail  where Year = " + nYearPlus1;
                SqlDataSource8.SelectCommand = "SELECT ID, CYBudget, USERID,UserName, AccountCode,AccountCodeDisplay,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Notes FROM vwForecastDetail where year = " + nYear + " and ID is not null  order by UserName";
                SqlDataSource9.SelectCommand = "SELECT USERID,AccountCode,AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonDetail where Year = " + nYear;
                SqlDataSource10.SelectCommand = "Select AccountID, AccountCode, Description, AssignedUser, ClassificationID,  Classification, Reviewer, AccountCodeDisplay from vwAccountUsers";
                SqlDataSource11.SelectCommand = "SELECT AccountCode,DATE,DATA,Detail FROM vwFinanceExport where Year = " + nYear + " order by AccountCode,DATE";
            }
            else
            {
                SqlDataSource7.SelectCommand = "";
                SqlDataSource8.SelectCommand = "";
                SqlDataSource9.SelectCommand = "";
                SqlDataSource10.SelectCommand = "";
                SqlDataSource11.SelectCommand = "";
            }
        }
        private void SetTabVisbility(string UserName)
        {
            bool m_bUser = false;
            bool m_bReviewer = false;
            bool m_bFinance = false;

            string selectSQL = "SELECT RoleName from vwUserRoles where UserID = '" + UserName + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strRole = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strRole = reader["RoleName"].ToString();
                    switch (strRole.ToUpper())
                    {
                        case "USER":
                            m_bUser = true;
                            break;
                        case "REVIEWER":
                            m_bReviewer = true; 
                            break;
                        case "FINANCE":
                            m_bFinance = true; 
                            break;
                    }
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            string strCurrentYearForecastText = "Current Year Forecast";
            string strNextYearBudget = "Next Year Budget";
            string strBudgetComparison = "Budget Comparison";
            string strCurrentYearBudget = "Current Year Budget";
            string strManagementBudgetReview = "Management Budget Review";
            string strManagementBudgetComparison = "Management Budget Comparison";
            string strFinanceBudgetReview = "Fin Budget Review";
            string strFinanceForecastReview = "Fin Forecast Review";
            string strFinanceBudgetComparison = "Fin Budget Comparison";
            string strFinanceAdmin = "Fin Admin";

            if (m_strYear == "2017")
            {
                strCurrentYearForecastText = "2017 Actuals";
                strNextYearBudget = "2018 Budget";

                strCurrentYearBudget = "2017 Budget";


                RadGrid3.MasterTableView.Columns[1].HeaderText = "2017 Budget";
                RadGrid3.MasterTableView.Columns[2].HeaderText = "2017 Actuals";
                RadGrid3.MasterTableView.Columns[3].HeaderText = "2017 Actuals - 2017 Budget";
                RadGrid3.MasterTableView.Columns[4].HeaderText = "2018 Budget";
                RadGrid3.MasterTableView.Columns[5].HeaderText = "2017 Actuals - 2018 Budget";

                RadGrid1.MasterTableView.Columns[13].HeaderText = "2017 Budget";
                RadGrid1.MasterTableView.Columns[14].HeaderText = "2017 Total";

                RadTab rootTabYTDAccountDetail = RadTabStrip1.FindTabByText("YTD Account Detail");
                rootTabYTDAccountDetail.Visible = false;

                RadGrid6.MasterTableView.Columns[1].HeaderText = "2017 Budget";
                RadGrid6.MasterTableView.Columns[2].HeaderText = "2017 Actuals";
                RadGrid6.MasterTableView.Columns[3].HeaderText = "2017 Actuals - 2017 Budget";
                RadGrid6.MasterTableView.Columns[4].HeaderText = "2018 Budget";
                RadGrid6.MasterTableView.Columns[5].HeaderText = "2017 Actuals - 2018 Budget";
            }
            else
            {
                strCurrentYearForecastText = "2018 Forecast";
                strNextYearBudget = "2019 Budget";

                strCurrentYearBudget = "2018 Budget";


                RadGrid3.MasterTableView.Columns[1].HeaderText = "2018 Budget";
                RadGrid3.MasterTableView.Columns[2].HeaderText = "2018 Forecast";
                RadGrid3.MasterTableView.Columns[3].HeaderText = "2018 Forecast - 2018 Budget";
                RadGrid3.MasterTableView.Columns[4].HeaderText = "2019 Budget";
                RadGrid3.MasterTableView.Columns[5].HeaderText = "2018 Forecast - 2019 Budget";

                RadGrid1.MasterTableView.Columns[13].HeaderText = "2018 Budget";
                RadGrid1.MasterTableView.Columns[14].HeaderText = "2018 Total";

                RadGrid8.MasterTableView.Columns[14].HeaderText = "2018 Budget";
                RadGrid8.MasterTableView.Columns[15].HeaderText = "2018 Total";

                RadGrid6.MasterTableView.Columns[2].HeaderText = "2018 Budget";
                RadGrid6.MasterTableView.Columns[3].HeaderText = "2018 Forecast";
                RadGrid6.MasterTableView.Columns[4].HeaderText = "2018 Forecast - 2018 Budget";
                RadGrid6.MasterTableView.Columns[5].HeaderText = "2019 Budget";
                RadGrid6.MasterTableView.Columns[6].HeaderText = "2018 Forecast - 2019 Budget";

            }


            RadTab rootTabUser = RadTabStrip1.Tabs[0];
            rootTabUser.Visible = m_bUser;
            m_strCurrentYearForecastText = strCurrentYearForecastText;
            rootTabUser.Text = m_strCurrentYearForecastText;
            rootTabUser = RadTabStrip1.Tabs[1];
            rootTabUser.Visible = m_bUser;
            m_strNextYearBudget = strNextYearBudget;
            rootTabUser.Text = m_strNextYearBudget;
            rootTabUser = RadTabStrip1.FindTabByText(m_strBudgetComparison);
            rootTabUser.Visible = m_bUser;
            rootTabUser = RadTabStrip1.Tabs[3];
            rootTabUser.Visible = m_bUser;
            m_strCurrentYearBudget = strCurrentYearBudget;
            rootTabUser.Text = m_strCurrentYearBudget;

            rootTabUser = RadTabStrip1.Tabs[10];
            rootTabUser.Visible = m_bUser;

            RadTab rootTabReviewer = RadTabStrip1.FindTabByText(m_strManagementBudgetReview);
            rootTabReviewer.Visible = m_bReviewer;
            rootTabReviewer = RadTabStrip1.FindTabByText(m_strManagementBudgetComparison);
            rootTabReviewer.Visible = m_bReviewer;
            RadTab rootTabFinance = RadTabStrip1.FindTabByText(m_strFinanceBudgetReview);
            rootTabFinance.Visible = m_bFinance;
            rootTabFinance = RadTabStrip1.FindTabByText(m_strFinanceForecastReview);
            rootTabFinance.Visible = m_bFinance;
            rootTabFinance = RadTabStrip1.FindTabByText(m_strFinanceBudgetComparison);
            rootTabFinance.Visible = m_bFinance;
            rootTabFinance = RadTabStrip1.FindTabByText(m_strFinanceAdmin);
            rootTabFinance.Visible = m_bFinance;

            if (m_bUser)
            {
                RadPageView1.Selected = true;
                rootTabUser = RadTabStrip1.FindTabByText(m_strCurrentYearForecastText);
                rootTabUser.Selected = true;
            }

            if (!m_bUser && m_bReviewer)
            {
                RadPageView5.Selected = true;
                rootTabReviewer = RadTabStrip1.FindTabByText(m_strManagementBudgetReview);
                rootTabReviewer.Selected = true;
            }
            if (!m_bUser && !m_bReviewer)
            {
                RadPageView6.Selected = true;
                rootTabFinance.Selected = true;
            }

            if (m_bFinance)
            {
                RadTab rootExport = RadTabStrip2.Tabs[1];
                rootExport.Selected = true;
                RadPageView12.Selected = true;
            }
            bool bAdminMode = Convert.ToBoolean(Session["ADMINMODE"]);

            if (bAdminMode)
            {
                if (cmbChangeUser.Items.Count == 0)
                    LoadUsers();

                lblChangeUser.Visible = false;
                cmbChangeUser.Visible = false;
            }
            else
            {
                lblChangeUser.Visible = false;
                cmbChangeUser.Visible = false;
            }

            SetSelectStatements();

            //int nYear = Convert.ToInt32(m_strYear);
            //int nYearPlus1 = Convert.ToInt32(m_strYear) + 1;
            //if (bUser)
            //{
            //    SqlDataSource1.SelectCommand = "SELECT ID, CYBudget, USERID,AccountCode,AccountCodeDisplay,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Notes FROM vwForecastDetail where Userid = '" + m_strUserName + "' and ID is not null and Year = " + nYear + " order by AccountCodeDisplay";
            //    SqlDataSource2.SelectCommand = "SELECT ID, USERID,Classification,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwBudgetDetail where Userid =  '" + m_strUserName + "' and Year =  " + nYearPlus1 + " order by AccountCodeDisplay";
            //    SqlDataSource3.SelectCommand = "SELECT USERID,AccountCode,AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonDetail where Userid = '" + m_strUserName + "' and Year =  " + nYear + " order by AccountCodeDisplay";
            //    SqlDataSource4.SelectCommand = "SELECT ID, USERID,Classification,AccountID,AccountCodeDisplay, AccountCode,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwCYBudgetDetail where Userid = '" + m_strUserName + "' and Year = " + nYearPlus1 + "  order by AccountCodeDisplay";
            //    SqlDataSource12.SelectCommand = "SELECT * FROM vwYTDActualLedger where Userid = '" + m_strUserName + "' order by date";
            //}

            //if (bReviewer)
            //{
            //    SqlDataSource5.SelectCommand = "SELECT ID, InputUserID,USERID,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail where Userid = '" + m_strUserName + "' and Year =  " + nYearPlus1 + " order by AccountCodeDisplay";
            //    SqlDataSource6.SelectCommand = "SELECT USERID,EmpName,AccountCode, AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonReviewDetail where UserID = '" + m_strUserName + "' and Year =  " + nYear + " order by EmpName";
            //}

            //if (bFinance)
            //{
            //    SqlDataSource7.SelectCommand = "SELECT ID, InputUserID,USERID,AccountID,AccountCodeDisplay,AccountCode,AccountCodeSingleLine,Detail,Quantity, UnitCost,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail  where Year = " + nYearPlus1;
            //    SqlDataSource8.SelectCommand = "SELECT ID, CYBudget, USERID,UserName, AccountCode,AccountCodeDisplay,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec, Notes FROM vwForecastDetail where year = " + nYear + " and ID is not null  order by UserName";
            //    SqlDataSource9.SelectCommand = "SELECT USERID,AccountCode,AccountCodeDisplay,CYBudget,CYForecast,NYBudget, VarianceComment FROM vwBudgetComparisonDetail where Year = " + nYear;
            //    SqlDataSource10.SelectCommand = "Select AccountID, AccountCode, Description, AssignedUser, ClassificationID,  Classification, Reviewer, AccountCodeDisplay from vwAccountUsers";
            //    SqlDataSource11.SelectCommand = "SELECT AccountCode,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec FROM vwReviewDetail";
            //}
            

            lblChangeYear.Visible = false;
            cmbChangeYear.Visible = false;
        }

 
        private void NotifyUser(string message)
        {

            RadListBoxItem commandListItem = new RadListBoxItem();

            commandListItem.Text = message;

        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            lblGrid.Style.Add("display", "none");

            if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource1.SelectCommand))
                return;

            GridTableView masterTable = RadGrid1.MasterTableView;
            GridColumn descriptionColumn = masterTable.GetColumnSafe("Notes") as GridColumn;
            (masterTable.GetBatchColumnEditor(descriptionColumn) as GridTextBoxColumnEditor).TextBoxControl.Width = Unit.Percentage(150);

            RadNumericTextBox descriptionEditor2 = null;

            if (!((GridBoundColumn)RadGrid1.Columns[1]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Jan") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }

            if (!((GridBoundColumn)RadGrid1.Columns[2]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Feb") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }

            if (!((GridBoundColumn)RadGrid1.Columns[3]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Mar") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }

            if (!((GridBoundColumn)RadGrid1.Columns[4]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Apr") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }

            if (!((GridBoundColumn)RadGrid1.Columns[5]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("May") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (!((GridBoundColumn)RadGrid1.Columns[6]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Jun") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (!((GridBoundColumn)RadGrid1.Columns[7]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Jul") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (!((GridBoundColumn)RadGrid1.Columns[8]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Aug") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (!((GridBoundColumn)RadGrid1.Columns[9]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Sep") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(70);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (!((GridBoundColumn)RadGrid1.Columns[10]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Oct") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (!((GridBoundColumn)RadGrid1.Columns[11]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Nov") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (!((GridBoundColumn)RadGrid1.Columns[12]).ReadOnly)
            {
                descriptionColumn = masterTable.GetColumnSafe("Dec") as GridColumn;
                descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                descriptionEditor2.Width = Unit.Percentage(100);
                descriptionEditor2.Style.Add("text-align", "right");
            }
            if (m_strYear == "2017")
            {
                ((GridBoundColumn)RadGrid1.Columns[0]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[1]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[2]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[3]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[4]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[5]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[6]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[7]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[8]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[9]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[10]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[11]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[12]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[13]).ReadOnly = true;

                ((GridBoundColumn)RadGrid1.Columns[0]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[1]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[2]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[3]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[4]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[5]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[6]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[7]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[8]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[9]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[10]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[11]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[12]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[13]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridCalculatedColumn)RadGrid1.Columns[14]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridCalculatedColumn)RadGrid1.Columns[15]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[16]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            }
            else
            {
                ((GridBoundColumn)RadGrid1.Columns[0]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[1]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[2]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[3]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[4]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[5]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[6]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[7]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[8]).ReadOnly = true;
                ((GridBoundColumn)RadGrid1.Columns[9]).ReadOnly = false;
                ((GridBoundColumn)RadGrid1.Columns[10]).ReadOnly = false;
                ((GridBoundColumn)RadGrid1.Columns[11]).ReadOnly = false;
                ((GridBoundColumn)RadGrid1.Columns[12]).ReadOnly = false;
                ((GridBoundColumn)RadGrid1.Columns[13]).ReadOnly = true;

                ((GridBoundColumn)RadGrid1.Columns[0]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[1]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[2]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[3]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[4]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[5]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[6]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[7]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[8]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[9]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid1.Columns[10]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid1.Columns[11]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid1.Columns[12]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid1.Columns[13]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridCalculatedColumn)RadGrid1.Columns[14]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridCalculatedColumn)RadGrid1.Columns[15]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid1.Columns[16]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            }


            RadGrid1.MasterTableView.PageSize = GetPageSize("CYFRows");
            RadGrid1.MasterTableView.Rebind();
        }

        protected void RadGrid2_PreRender(object sender, EventArgs e)

        {
            if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource2.SelectCommand))
                return;

            bool bConsiliumLocked = Convert.ToBoolean(Session["CONSILIUMLOCKED"]);
            bool bAdminMode = Convert.ToBoolean(Session["ADMINMODE"]);
            if ((bAdminMode || !bConsiliumLocked) && m_strYear == "2018")
            {


                GridTableView masterTable = RadGrid2.MasterTableView;
                GridColumn descriptionColumn = masterTable.GetColumnSafe("Detail") as GridColumn;
                try
                {
                    (masterTable.GetBatchColumnEditor(descriptionColumn) as GridTextBoxColumnEditor).TextBoxControl.Width = Unit.Percentage(110);
                }
                catch { }

                descriptionColumn = masterTable.GetColumnSafe("Quantity") as GridColumn;
                try
                {
                    RadNumericTextBox descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("UnitCost") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Jan") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Feb") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Mar") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Apr") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("May") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Jun") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Jul") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Aug") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Sep") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Oct") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Nov") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                    descriptionColumn = masterTable.GetColumnSafe("Dec") as GridColumn;
                    descriptionEditor2 = (masterTable.GetBatchColumnEditor(descriptionColumn) as GridNumericColumnEditor).NumericTextBox;
                    descriptionEditor2.Width = Unit.Percentage(100);
                    descriptionEditor2.Style.Add("text-align", "right");

                }
                catch
                {

                }

                ((GridTemplateColumn)RadGrid2.Columns[0]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[1]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[2]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[3]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[6]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[7]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[8]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[9]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[10]).ReadOnly = false;

                ((GridBoundColumn)RadGrid2.Columns[11]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[12]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[13]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[14]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[15]).ReadOnly = false;
                ((GridBoundColumn)RadGrid2.Columns[16]).ReadOnly = false;

                ((GridTemplateColumn)RadGrid2.Columns[0]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[1]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[2]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[3]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridCalculatedColumn)RadGrid2.Columns[4]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[6]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[7]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[8]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[9]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[10]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;

                ((GridBoundColumn)RadGrid2.Columns[11]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[12]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[13]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[14]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[15]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[16]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
                ((GridBoundColumn)RadGrid2.Columns[17]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;

                RadGrid2.MasterTableView.PageSize = GetPageSize("NYBRows");
                RadGrid2.MasterTableView.Rebind();

            }
            else
            {
                ((GridTemplateColumn)RadGrid2.Columns[0]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[1]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[2]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[3]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[6]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[7]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[8]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[9]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[10]).ReadOnly = true;

                ((GridBoundColumn)RadGrid2.Columns[11]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[12]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[13]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[14]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[15]).ReadOnly = true;
                ((GridBoundColumn)RadGrid2.Columns[16]).ReadOnly = true;

                ((GridTemplateColumn)RadGrid2.Columns[0]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[1]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[2]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[3]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridCalculatedColumn)RadGrid2.Columns[4]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[6]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[7]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[8]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[9]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[10]).ItemStyle.BackColor = System.Drawing.Color.White;

                ((GridBoundColumn)RadGrid2.Columns[11]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[12]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[13]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[14]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[15]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[16]).ItemStyle.BackColor = System.Drawing.Color.White;
                ((GridBoundColumn)RadGrid2.Columns[17]).ItemStyle.BackColor = System.Drawing.Color.White;

                RadGrid2.Columns[5].Visible = false;
                RadGrid2.Columns[19].Visible = false;
                RadGrid2.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
                RadGrid2.MasterTableView.PageSize = GetPageSize("NYBRows");
                RadGrid2.MasterTableView.Rebind();
            }

        }


        protected void RadGrid3_PreRender(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource3.SelectCommand))
                return;

            GridTableView masterTable = RadGrid3.MasterTableView;
            GridColumn descriptionColumn = masterTable.GetColumnSafe("VarianceComment") as GridColumn;
            (masterTable.GetBatchColumnEditor(descriptionColumn) as GridTextBoxColumnEditor).TextBoxControl.Width = Unit.Percentage(110);

            ((GridBoundColumn)RadGrid3.Columns[6]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;

            RadGrid3.MasterTableView.PageSize = GetPageSize("BCRows");
            RadGrid3.MasterTableView.Rebind();
        }

        protected void RadGrid4_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource4.SelectCommand))
                    return;

                RadGrid4.MasterTableView.PageSize = GetPageSize("CYBRows");
                RadGrid4.MasterTableView.Rebind();
            }
            catch { }

        }

        protected void RadGrid5_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource5.SelectCommand))
                    return;

                RadGrid5.MasterTableView.PageSize = GetPageSize("MBRRows");
                RadGrid5.MasterTableView.Rebind();
            }
            catch { }
        }

        protected void RadGrid6_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource6.SelectCommand))
                    return;

                RadGrid6.MasterTableView.PageSize = GetPageSize("MBCRows");
                RadGrid6.MasterTableView.Rebind();
            }
            catch { }
        }

        protected void RadGrid7_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource7.SelectCommand))
                    return;

                RadGrid7.MasterTableView.PageSize = GetPageSize("FBRRows");
                RadGrid7.MasterTableView.Rebind();
            }
            catch { }
        }

        protected void RadGrid8_PreRender(object sender, EventArgs e)
        {
                try
                {
                    if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource8.SelectCommand))
                        return;

                    RadGrid8.MasterTableView.PageSize = GetPageSize("FCRRows");
                    RadGrid8.MasterTableView.Rebind();
                }
                catch { }
        }

        protected void RadGrid9_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource9.SelectCommand))
                    return;

                RadGrid9.MasterTableView.PageSize = GetPageSize("FBCRows");
                RadGrid9.MasterTableView.Rebind();
            }
            catch { }
        }

        protected void RadGrid10_PreRender(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(m_strUserID) || string.IsNullOrEmpty(SqlDataSource10.SelectCommand))
                return;

            //GridTableView masterTable = RadGrid10.MasterTableView;
            //GridColumn descriptionColumn = masterTable.GetColumnSafe("AccountCode") as GridColumn;
            //(masterTable.GetBatchColumnEditor(descriptionColumn) as GridTextBoxColumnEditor).TextBoxControl.Width = Unit.Percentage(110);
            //descriptionColumn = masterTable.GetColumnSafe("Description") as GridColumn;
            //(masterTable.GetBatchColumnEditor(descriptionColumn) as GridTextBoxColumnEditor).TextBoxControl.Width = Unit.Percentage(110);


            //((GridBoundColumn)RadGrid10.Columns[0]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            //((GridBoundColumn)RadGrid10.Columns[1]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            //((GridTemplateColumn)RadGrid10.Columns[3]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            //((GridTemplateColumn)RadGrid10.Columns[4]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
            //((GridTemplateColumn)RadGrid10.Columns[5]).ItemStyle.BackColor = System.Drawing.Color.LightBlue;
        }

        protected void SqlDataSource1_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            DbParameterCollection CmdParams = e.Command.Parameters;
            ParameterCollection UpdParams = ((SqlDataSourceView)sender).UpdateParameters;

            Hashtable ht = new Hashtable();
            foreach (Parameter UpdParam in UpdParams)
                ht.Add(UpdParam.Name, true);

            for (int i = 0; i < CmdParams.Count; i++)
            {
                if (!ht.Contains(CmdParams[i].ParameterName.Substring(1)))
                    CmdParams.Remove(CmdParams[i--]);
            }
            RadGrid1.MasterTableView.PageSize = GetPageSize("CYFRows");
        }

        protected void SqlDataSource2_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            DbParameterCollection CmdParams = e.Command.Parameters;
            ParameterCollection UpdParams = ((SqlDataSourceView)sender).UpdateParameters;

            Hashtable ht = new Hashtable();
            foreach (Parameter UpdParam in UpdParams)
                ht.Add(UpdParam.Name.ToUpper(), true);

            CmdParams["@AccountCodeDisplay"].Value = CmdParams["@AccountID"].Value;

            for (int i = 0; i < CmdParams.Count; i++)
            {
                if (!ht.Contains(CmdParams[i].ParameterName.Substring(1).ToUpper()))
                    CmdParams.Remove(CmdParams[i--]);
            }
        }

        protected void SqlDataSource3_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            DbParameterCollection CmdParams = e.Command.Parameters;
            ParameterCollection UpdParams = ((SqlDataSourceView)sender).UpdateParameters;

            Hashtable ht = new Hashtable();
            foreach (Parameter UpdParam in UpdParams)
                ht.Add(UpdParam.Name, true);

            for (int i = 0; i < CmdParams.Count; i++)
            {
                if (!ht.Contains(CmdParams[i].ParameterName.Substring(1)))
                    CmdParams.Remove(CmdParams[i--]);
            }
        }

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();
                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid2_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();
                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid2.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid2.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid2.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid3_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();
                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid3.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid3.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid3.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid4_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();

                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid4.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid4.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid4.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid5_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();

                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid5.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid5.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid5.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid6_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();
                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid6.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid6.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid6.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid7_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();
                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid7.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid7.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid7.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid8_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();

                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid8.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid8.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid8.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }

        protected void RadGrid9_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();
                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid9.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid9.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid9.MasterTableView.ClientID);
                try
                {
                    PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
                }
                catch
                {
                    PageSizeCombo.FindItemByText("100").Selected = true;
                }
            }
        }


        protected void RadGrid5_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //RadGrid5.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            //RadGrid5.ExportSettings.IgnorePaging = true;
            //RadGrid5.ExportSettings.ExportOnlyData = true;
            //RadGrid5.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid6_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //adGrid6.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            //RadGrid6.ExportSettings.IgnorePaging = true;
            //RadGrid6.ExportSettings.ExportOnlyData = true;
            //RadGrid6.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid7_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //RadGrid7.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            //RadGrid7.ExportSettings.IgnorePaging = true;
            //RadGrid7.ExportSettings.ExportOnlyData = true;
            //RadGrid7.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid8_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //RadGrid8.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            //RadGrid8.ExportSettings.IgnorePaging = true;
            //RadGrid8.ExportSettings.ExportOnlyData = true;
            //RadGrid8.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid9_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //RadGrid9.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            //RadGrid9.ExportSettings.IgnorePaging = true;
            //RadGrid9.ExportSettings.ExportOnlyData = true;
            //RadGrid9.ExportSettings.OpenInNewWindow = true;
        }

        

        protected void cmbChangeUser_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string CurrentUserName = cmbChangeUser.SelectedItem.Value;
            m_strUserName = CurrentUserName;
            Session["USERNAME"] = m_strUserName;

            string strUserID = GetUserID(CurrentUserName);
            m_strUserID = strUserID;
            Session["USERID"] = m_strUserID;

            if (!string.IsNullOrEmpty(CurrentUserName))
            {
                GetLoggedInUser();
                SetTabVisbility(CurrentUserName);
                
            }
            else
                Response.Redirect("~\\About.aspx");
        }



        protected void chkLockDown_CheckedChanged(object sender, EventArgs e)
        {
            string selectSQL = "Update AdminSettings Set ConsiliumLocked = '" + chkLockDown.Checked + "'";

            myConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(myConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);

            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        protected void RadGrid1_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid1.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("CYFRows", e.NewPageSize);
        }

        protected void RadGrid2_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid2.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("NYBRows", e.NewPageSize);
        }

        protected void RadGrid3_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid3.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("BCRows", e.NewPageSize);
        }

        protected void RadGrid4_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid4.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("CYBRows", e.NewPageSize);
        }

        protected void RadGrid5_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid5.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("MBRRows", e.NewPageSize);
        }

        protected void RadGrid6_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid6.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("MBCRows", e.NewPageSize);
        }

        protected void RadGrid7_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid7.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("FBRRows", e.NewPageSize);
        }

        protected void RadGrid8_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid8.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("FCRRows", e.NewPageSize);
        }

        protected void RadGrid9_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid9.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("FBCRows", e.NewPageSize);
        }

        private int GetPageSize(string strGrid)
        {
            Session["USERID"] = m_strUserID;
            string selectSQL = "SELECT " + strGrid + " from Users where ID = " + Session["USERID"];

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strRows = string.Empty;


            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strRows = reader[strGrid].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            int nIndex = 100;
            try
            {
                nIndex = Convert.ToInt16(strRows);
            }
            catch
            {
                nIndex = 100;
            }

            if (nIndex < 100)
                return 100;
            else
                return nIndex;
        }

        private void SetPageSize(string strGrid, int PageIndex)
        {
            Session["USERID"] = m_strUserID;
            string selectSQL = "Update Users Set " + strGrid + " = " + PageIndex + " where ID = " + Session["USERID"];

            myConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(myConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);

            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

        }

        protected void RadGrid11_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //RadGrid11.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            //RadGrid11.ExportSettings.IgnorePaging = true;
            //RadGrid11.ExportSettings.ExportOnlyData = true;
            //RadGrid11.ExportSettings.OpenInNewWindow = true;
        }

        protected void cmbChangeYear_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (cmbChangeYear.SelectedIndex == 1)
                Session["YEAR"] = "2017";
            else
                Session["YEAR"] = "2018";
            m_strYear = Session["YEAR"].ToString();
            SetTabVisbility(m_strUserName);
        }

        protected void SqlDataSource2_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            //DbParameterCollection CmdParams = e.Command.Parameters;
            //ParameterCollection UpdParams = ((SqlDataSourceView)sender).UpdateParameters;

            //Hashtable ht = new Hashtable();
            //foreach (Parameter UpdParam in UpdParams)
            //    ht.Add(UpdParam.Name, true);

            //for (int i = 0; i < CmdParams.Count; i++)
            //{
            //    if (!ht.Contains(CmdParams[i].ParameterName.Substring(1)))
            //        CmdParams.Remove(CmdParams[i--]);
            //}
        }

        protected void btnSortAccountCode_Click(object sender, EventArgs e)
        {
            GridSortExpression expression = new GridSortExpression();
            expression.FieldName = "AccountCode";
            expression.SortOrder = GridSortOrder.Ascending;
            RadGrid1.MasterTableView.SortExpressions.AddSortExpression(expression);
            RadGrid1.MasterTableView.Rebind();
        }

        protected void btnSortAccountCode2_Click(object sender, EventArgs e)
        {
            GridSortExpression expression = new GridSortExpression();
            expression.FieldName = "AccountCode";
            expression.SortOrder = GridSortOrder.Ascending;
            RadGrid2.MasterTableView.SortExpressions.AddSortExpression(expression);
            RadGrid2.MasterTableView.Rebind();
        }

        protected void btnSortAccountCode3_Click(object sender, EventArgs e)
        {
            GridSortExpression expression = new GridSortExpression();
            expression.FieldName = "AccountCode";
            expression.SortOrder = GridSortOrder.Ascending;
            RadGrid3.MasterTableView.SortExpressions.AddSortExpression(expression);
            RadGrid3.MasterTableView.Rebind();
        }

        protected void btnSortAccountCode4_Click(object sender, EventArgs e)
        {
            GridSortExpression expression = new GridSortExpression();
            expression.FieldName = "AccountCode";
            expression.SortOrder = GridSortOrder.Ascending;
            RadGrid4.MasterTableView.SortExpressions.AddSortExpression(expression);
            RadGrid4.MasterTableView.Rebind();
        }
    }
}